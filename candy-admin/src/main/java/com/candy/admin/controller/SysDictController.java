package com.candy.admin.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.*;
import com.candy.biz.domain.vo.DictSelectItemVO;
import com.candy.biz.domain.vo.SysDictItemVO;
import com.candy.biz.domain.vo.SysDictVO;
import com.candy.biz.service.SysDictItemService;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import com.candy.common.enums.RedisKeyEnum;
import com.candy.common.manager.RedisManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysDictService;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 系统字典-控制层。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Tag(name = "系统字典")
@RestController
@RequestMapping("/sys-dict")
@AllArgsConstructor
public class SysDictController {
    private final static String MODULE_NAME = "系统字典";
    private final SysDictService sysDictService;
    private final SysDictItemService sysDictItemService;

    /**
     * 查询系统字典列表。
     *
     * @param param 查询条件
     * @return 字典列表
     */
    @Operation(summary = "列表",description = "查询系统字典列表")
    @SaCheckPermission("sys:dict:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统字典列表",saveResponse = false)
    @GetMapping("list")
    public Result<List<SysDictVO>> queryList(SysDictQueryParam param) {
        return Result.success(sysDictService.queryList(param));
    }

    /**
     * 查询系统字典详情。
     *
     * @param id 字典主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "根据字典主键获取详细信息")
    @Parameter(name = "id",description = "字典id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:dict:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统字典详情")
    @GetMapping("info/{id}")
    public Result<SysDictVO> queryInfo(@PathVariable("id") Long id) {
        return Result.success(sysDictService.queryInfo(id));
    }

    /**
     * 分页查询系统字典。
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询系统字典")
    @SaCheckPermission("sys:dict:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询系统字典",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysDictVO>> queryPage(SysDictQueryParam param) {
        return Result.success(sysDictService.queryPage(param));
    }

    /**
     * 查询系统字典项列表
     *
     * @param param 查询条件
     * @return 字典项列表
     */
    @Operation(summary = "字典项列表查询",description = "查询系统字典项列表")
    @SaCheckPermission(value = {"sys:dict:info", "sys:dict:item"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统字典项列表",saveResponse = false)
    @GetMapping("item-list")
    public Result<List<SysDictItemVO>> queryItemList(SysDictItemQueryParam param) {
        return Result.success(sysDictItemService.queryList(param));
    }

    /**
     * 查询字典下拉框选项
     *
     * @param dictCode 字典code
     * @return 字典项列表
     */
    @Operation(summary = "字典下拉框选项查询",description = "字典下拉框选项查询")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询字典下拉框选项",saveResponse = false)
    @GetMapping("item-select/{dictCode}")
    public Result<List<DictSelectItemVO>> queryDictSelectItem(@PathVariable("dictCode") String dictCode) {
        return Result.success(sysDictItemService.queryDictSelectItem(dictCode));
    }


    /**
     * 保存字典
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "字典保存",description = "新增或修改字典")
    @SaCheckPermission(value = {"sys:dict:add", "sys:dict:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE)
    @PostMapping("save")
    public Result<Boolean> saveHandle(@RequestBody @Validated SysDictSaveParam param) {
        return Result.success(sysDictService.saveHandle(param));
    }

    /**
     * 保存字典项
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "字典项保存",description = "新增或修改字典项")
    @SaCheckPermission("sys:dict:item")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE)
    @PostMapping("save-item")
    public Result<Boolean> saveItemHandle(@RequestBody @Validated SysDictItemSaveParam param) {
        return Result.success(sysDictItemService.saveHandle(param));
    }

    /**
     * 根据主键删除字典
     *
     * @param id 主键
     * @return 删除结果
     */
    @Operation(summary = "字典删除",description = "根据主键删除字典")
    @Parameter(name = "id",description = "字典id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:dict:remove")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> removeHandler(@PathVariable Long id) {
        RedisManager.deleteByPrefix(RedisKeyEnum.DICT_MAP.getKey());
        return Result.success(sysDictService.removeHandle(id));
    }




}
