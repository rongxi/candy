package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.SysRoleAssigningMenusParam;
import com.candy.biz.domain.param.SysRoleQueryParam;
import com.candy.biz.domain.param.SysRoleSaveParam;
import com.candy.biz.domain.vo.SysRoleVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysRoleService;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 系统角色-控制层。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Tag(name = "系统角色")
@RestController
@RequestMapping("/sys-role")
@AllArgsConstructor
public class SysRoleController {
    private final static String MODULE_NAME = "系统角色";
    private final SysRoleService sysRoleService;

    /**
     * 根据系统角色主键获取详细信息。
     *
     * @param id 系统角色主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "根据主键获取详细信息")
    @Parameter(name = "id",description = "系统角色id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:role:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统角色详情")
    @GetMapping("info/{id}")
    public Result<SysRoleVO> queryInfo(@PathVariable Long id) {
        return Result.success(sysRoleService.queryInfo(id));
    }

    /**
     * 分页查询系统角色。
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "根据条件分页查询系统角色列表")
    @SaCheckPermission("sys:role:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询系统角色",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysRoleVO>> queryPage(SysRoleQueryParam param) {
        return Result.success(sysRoleService.queryPage(param));
    }

    /**
     * 根据条件查询系统角色列表
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @Operation(summary = "查询列表",description = "根据条件查询系统角色列表")
    @SaCheckPermission("sys:role:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统角色列表",saveResponse = false)
    @GetMapping("list")
    public Result<List<SysRoleVO>> queryList(SysRoleQueryParam param) {
        return Result.success(sysRoleService.queryList(param));
    }

    /**
     * 导出系统角色
     *
     * @param param 查询参数
     */
    @Operation(summary = "excel导出",description = "根据查询条件导出系统角色")
    @SaCheckPermission("sys:role:export")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysRoleQueryParam param) {
        sysRoleService.export(param);
    }

    /**
     * 查询已分配的菜单权限
     *
     * @param id 角色id
     * @return 已分配菜单id列表
     */
    @Operation(summary = "查询已分配的菜单权限",description = "查询已分配的菜单权限")
    @SaCheckPermission("sys:role:menu")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询已分配的菜单权限")
    @GetMapping("assigned-menu/{id}")
    public Result<List<Long>> queryAssignedMenu(@PathVariable Long id) {
        return Result.success(sysRoleService.queryAssignedMenu(id));
    }

    /**
     * 保存系统角色
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "保存",description = "保存系统角色")
    @SaCheckPermission(value = {"sys:role:add", "sys:role:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE)
    @PostMapping("save")
    public Result<Boolean> saveHandle(@RequestBody @Validated SysRoleSaveParam param) {
        return Result.success(sysRoleService.saveHandle(param));
    }

    /**
     * 删除系统角色。
     *
     * @param id 系统角色主键
     * @return 删除结果
     */
    @Operation(summary = "删除",description = "删除系统角色")
    @Parameter(name = "id",description = "系统角色id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:role:remove")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> remove(@PathVariable Long id) {
        return Result.success(sysRoleService.removeRole(id));
    }


    /**
     * 保存角色的菜单权限
     *
     * @param param 菜单ids
     * @return 保存结果
     */
    @Operation(summary = "分配菜单权限",description = "保存角色的菜单权限")
    @SaCheckPermission("sys:role:menu")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.UPDATE,describe = "保存角色的菜单权限")
    @PostMapping("assigning-menus")
    public Result<Boolean> assigningMenu(@RequestBody @Validated SysRoleAssigningMenusParam param ) {
        return Result.success(sysRoleService.assigningMenu(param));
    }

}
