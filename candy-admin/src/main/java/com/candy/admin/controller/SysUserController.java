package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.*;
import com.candy.biz.domain.vo.SysUserLoginVO;
import com.candy.biz.domain.vo.SysUserVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysUserService;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 系统用户-控制层。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Tag(name = "系统用户")
@RestController
@RequestMapping("/sys-user")
@AllArgsConstructor
public class SysUserController {
    private final static String MODULE_NAME = "系统用户";
    private final SysUserService sysUserService;

    /**
     * 查询系统用户详情
     *
     * @param id 系统用户表主键
     * @return 系统用户表详情
     */
    @Operation(summary = "详情",description = "根据主键获取详细信息")
    @Parameter(name = "id",description = "系统用户id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:user:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统用户详情")
    @GetMapping("info/{id}")
    public Result<SysUserVO> queryInfo(@PathVariable Long id) {
        return Result.success(sysUserService.queryInfo(id));
    }

    /**
     * 分页查询系统用户
     *
     * @param param 查询对象
     * @return 分页对象
     */
    @Operation(summary = "分页查询",description = "根据条件分页查询系统用户列表")
    @SaCheckPermission("sys:user:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询系统用户", saveRequest = false)
    @GetMapping("page")
    public Result<PageVO<SysUserVO>> queryPage(SysUserQueryParam param) {
        return Result.success(sysUserService.queryPage(param));
    }

    /**
     * 查询用户已分配的角色
     *
     * @param id 用户id
     * @return 已分配角色
     */
    @Operation(summary = "查询已分配角色",description = "查询已分配角色")
    @SaCheckPermission("sys:user:role")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统用户角色")
    @GetMapping("assigned-role/{id}")
    public Result<List<Long>> queryAssignedRole(@PathVariable Long id) {
        return Result.success(sysUserService.queryAssignedRole(id));
    }

    /**
     * 保存系统用户
     *
     * @param param 系统用户参数
     * @return 添加或修改结果
     */
    @Operation(summary = "保存",description = "保存系统用户")
    @SaCheckPermission(value = {"sys:user:add", "sys:user:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE)
    @PostMapping("save")
    public Result<Boolean> saveHandle(@RequestBody @Validated SysUserSaveParam param) {
        return Result.success(sysUserService.saveHandle(param));
    }

    /**
     * 删除系统用户
     *
     * @param id 主键
     * @return 删除结果
     */
    @Operation(summary = "删除",description = "删除系统用户")
    @Parameter(name = "id",description = "系统用户id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:user:remove")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> remove(@PathVariable Long id) {
        return Result.success(sysUserService.removeUser(id));
    }

    /**
     * 导出系统用户
     *
     * @param param 查询参数
     */
    @Operation(summary = "excel导出",description = "导出系统用户")
    @SaCheckPermission("sys:user:export")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysUserQueryParam param) {
        sysUserService.export(param);
    }

    /**
     * 修改用户状态
     *
     * @param param 更新参数
     * @return 修改状态结果
     */
    @Operation(summary = "修改状态",description = "修改用户状态")
    @SaCheckPermission("sys:user:edit")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.UPDATE,describe = "修改用户状态")
    @PostMapping("update-state")
    public Result<Boolean> updateState(@RequestBody @Validated SysUserUpdateStateParam param) {
        return Result.success(sysUserService.updateState(param));
    }

    /**
     * 重置密码
     *
     * @param param 更新参数
     * @return 重置密码结果
     */
    @Operation(summary = "重置登录密码",description = "重置登录密码")
    @SaCheckPermission("sys:user:reset-pwd")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.UPDATE,describe = "重置密码")
    @PostMapping("reset-password")
    public Result<Boolean> resetPassword(@RequestBody @Validated SysUserResetPasswordParam param) {
        return Result.success(sysUserService.resetPassword(param));
    }

    /**
     * 登录
     *
     * @param param 登录参数
     * @return 用户信息
     */
    @Operation(summary = "登录",description = "登录")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.LOGIN,describe = "登录")
    @PostMapping("login")
    public Result<SysUserLoginVO> login(@RequestBody @Validated SysUserLoginParam param) {
        return Result.success(sysUserService.login(param));
    }

    /**
     * 退出登录
     *
     * @return 退出结果
     */
    @Operation(summary = "退出",description = "退出登录")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.LOGOUT,describe = "退出登录")
    @PostMapping("login-out")
    public Result<Boolean> loginOut() {
        return Result.success(sysUserService.loginOut());
    }


    /**
     * 获取登录验证码图片
     *
     * @param clientId 客户端id
     */
    @Operation(summary = "获取登录验证码图片",description = "获取登录验证码图片")
    @Parameter(name = "clientId",description = "客户端id",in = ParameterIn.PATH)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "获取登录验证码图片")
    @GetMapping("captcha-img/{clientId}")
    public void getCaptchaImg(@PathVariable String clientId) {
        sysUserService.getCaptchaImg(clientId);
    }


    /**
     * 保存用户分配的角色
     *
     * @param param 角色ids
     * @return 保存结果
     */
    @Operation(summary = "分配角色",description = "分配角色")
    @SaCheckPermission("sys:user:role")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.UPDATE,describe = "保存分配角色")
    @PostMapping("assigning-roles")
    public Result<Boolean> assigningRoles(@RequestBody @Validated SysUserAssigningRolesParam param ) {
        return Result.success(sysUserService.assigningRoles(param));
    }



}
