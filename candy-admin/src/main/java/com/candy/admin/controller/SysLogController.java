package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.SysLogQueryParam;
import com.candy.biz.domain.param.SysLogSaveParam;
import com.candy.biz.domain.vo.OnlineUserVO;
import com.candy.biz.domain.vo.SysLogVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysLogService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 系统操作日志-控制层。
 *
 * @author rong xi
 * @date 2023/10/11 16:59
 * @version 1.0.0
 */
@Tag(name = "系统操作日志")
@RestController
@RequestMapping("/sys-log")
@AllArgsConstructor
public class SysLogController {
    private final static String MODULE_NAME = "系统操作日志";
    private final SysLogService sysLogService;

    /**
     * 查询系统操作日志详情
     *
     * @param id 系统操作日志主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "查询系统操作日志详情")
    @Parameter(name = "id",description = "系统操作日志id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:log:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统操作日志详情")
    @GetMapping("info/{id}")
    public Result<SysLogVO> queryInfo(@PathVariable("id") Long id) {
        return Result.success(sysLogService.queryInfo(id));
    }

    /**
     * 分页查询系统操作日志
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询系统操作日志")
    @SaCheckPermission("sys:log:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询系统操作日志",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysLogVO>> queryPage(SysLogQueryParam param) {
        return Result.success(sysLogService.queryPage(param));
    }


    /**
     * 导出系统操作日志
     *
     * @param param 查询参数
     */
    @Operation(summary = "数据导出",description = "导出系统操作日志")
    @SaCheckPermission("sys:log:export")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysLogQueryParam param) {
        sysLogService.export(param);
    }

    /**
     * 保存系统操作日志
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "保存",description = "保存系统操作日志")
    @SaCheckPermission(value = {"sys:log:add", "sys:log:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE)
    @PostMapping(value="save",produces = MediaType.APPLICATION_JSON_VALUE)
    public Result<Boolean> saveHandle(@RequestBody @Validated SysLogSaveParam param) {
        return Result.success(sysLogService.saveHandle(param));
    }



}
