package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.stp.StpUtil;
import com.candy.biz.domain.param.SysMenuQueryParam;
import com.candy.biz.domain.param.SysMenuSaveParam;
import com.candy.biz.domain.vo.SysMenuVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysMenuService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import cn.hutool.core.lang.tree.Tree;

/**
 * 菜单权限-控制层。
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Tag(name = "菜单权限")
@RestController
@RequestMapping("/sys-menu")
@AllArgsConstructor
public class SysMenuController {
    private final static String MODULE_NAME = "系统菜单";
    private final SysMenuService sysMenuService;

    /**
     * 查询菜单权限详情。
     *
     * @param id 菜单权限主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "根据菜单权限主键获取详细信息")
    @Parameter(name = "id",description = "菜单权限id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:menu:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询菜单权限详情")
    @GetMapping("info/{id}")
    public Result<SysMenuVO> queryInfo(@PathVariable("id") Long id) {
        return Result.success(sysMenuService.queryInfo(id));
    }

    /**
     * 分页查询菜单权限。
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询菜单权限")
    @SaCheckPermission("sys:menu:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询菜单权限",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysMenuVO>> queryPage(SysMenuQueryParam param) {
        return Result.success(sysMenuService.queryPage(param));
    }

    /**
     * 查询菜单树列表。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @Operation(summary = "查询树列表",description = "查询菜单树列表")
    @SaCheckPermission("sys:menu:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询菜单树列表",saveResponse = false)
    @GetMapping("tree-list")
    public Result<List<Tree<Long>>> queryTreeList(SysMenuQueryParam param) {
        return Result.success(sysMenuService.queryTreeList(param));
    }

    /**
     * 查询菜单树对象。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @Operation(summary = "查询树对象",description = "查询菜单树对象")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询菜单树对象")
    @GetMapping("tree")
    public Result<Tree<Long>> queryTree(SysMenuQueryParam param) {
        return Result.success(sysMenuService.queryTree(param));
    }

    /**
    * 导出菜单权限
    *
    * @param param 查询参数
    */
    @Operation(summary = "导出",description = "导出菜单权限")
    @SaCheckPermission("sys:menu:export")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysMenuQueryParam param) {
        sysMenuService.export(param);
    }

    /**
     * 保存菜单权限
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "保存",description = "保存菜单权限")
    @SaCheckPermission(value = {"sys:menu:add", "sys:menu:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT)
    @PostMapping(value="save",produces = MediaType.APPLICATION_JSON_VALUE)
    public Result<Boolean> saveHandle(@RequestBody @Validated SysMenuSaveParam param) {
        return Result.success(sysMenuService.saveHandle(param));
    }

    /**
     * 删除菜单权限。
     *
     * @param id 菜单权限主键
     * @return 删除结果
     */
    @Operation(summary = "删除",description = "删除菜单权限")
    @Parameter(name = "id",description = "菜单权限id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:menu:remove")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> remove(@PathVariable("id") Long id) {
        return Result.success(sysMenuService.removeById(id));
    }


    /**
     * 查询菜单列表。
     *
     * @return 查询结果
     */
    @Operation(summary = "查询菜单树列表",description = "查询菜单树列表")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询菜单树列表",saveResponse = false)
    @GetMapping("/menus")
    public Result<List<Tree<String>>> menus() {
        return Result.success(sysMenuService.queryMenus(StpUtil.getLoginIdAsLong()));
    }

    /**
     * 查询权限列表。
     *
     * @return 查询结果
     */
    @Operation(summary = "查询权限列表",description = "查询权限列表")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询权限列表")
    @GetMapping("/permissions")
    public Result<List<String>> permissions() {
        return Result.success(sysMenuService.queryPermissions(StpUtil.getLoginIdAsLong()));
    }


}
