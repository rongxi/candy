package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.SysConfigQueryParam;
import com.candy.biz.domain.param.SysConfigSaveParam;
import com.candy.biz.domain.vo.SysConfigVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import com.candy.common.enums.RedisKeyEnum;
import com.candy.common.manager.RedisManager;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysConfigService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 参数配置-控制层。
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Tag(name = "参数配置")
@RestController
@RequestMapping("/sys-config")
@AllArgsConstructor
public class SysConfigController {
    private final static String MODULE_NAME = "系统参数配置";
    private final SysConfigService sysConfigService;

    /**
     * 查询系统参数配置详细
     *
     * @param id 参数配置主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "查询系统参数配置详细")
    @Parameter(name = "id",description = "参数配置id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:config:info")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "查询系统参数配置详细")
    @GetMapping("info/{id}")
    public Result<SysConfigVO> queryInfo(@PathVariable("id") Long id) {
        return Result.success(sysConfigService.queryInfo(id));
    }

    /**
     * 查询系统参数配置详细
     *
     * @param key 参数配置key
     * @return 配置值
     */
    @Operation(summary = "获取配置值",description = "获取配置值")
    @Parameter(name = "key",description = "参数配置key" ,in = ParameterIn.PATH)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "获取配置值")
    @GetMapping("get-value/{key}")
    public Result<String> getValue(@PathVariable("key") String key) {
        return Result.success(sysConfigService.getValue(key));
    }

    /**
     * 分页查询系统参数配置
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询系统参数配置")
    @SaCheckPermission("sys:config:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询系统参数配置",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysConfigVO>> queryPage(SysConfigQueryParam param) {
        return Result.success(sysConfigService.queryPage(param));
    }

    /**
    * 导出参数配置
    *
    * @param param 查询参数
    */
    @Operation(summary = "数据导出",description = "导出参数配置")
    @SaCheckPermission("sys:config:export")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysConfigQueryParam param) {
        sysConfigService.export(param);
    }

    /**
     * 保存系统参数配置
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "保存",description = "保存系统参数配置")
    @SaCheckPermission(value = {"sys:config:add", "sys:config:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME, action = LogActionEnum.SAVE,describe = "保存系统参数配置")
    @PostMapping(value="save",produces = MediaType.APPLICATION_JSON_VALUE)
    public Result<Boolean> saveHandle(@RequestBody @Validated SysConfigSaveParam param) {
        return Result.success(sysConfigService.saveHandle(param));
    }

    /**
     * 删除参数配置
     *
     * @param id 参数配置主键
     * @return 删除结果
     */
    @Operation(summary = "删除",description = "删除参数配置")
    @Parameter(name = "id",description = "参数配置id",in = ParameterIn.PATH)
    @SaCheckPermission("sys:config:remove")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> remove(@PathVariable("id") Long id) {
        RedisManager.delete(RedisKeyEnum.CONFIG.getKey());
        return Result.success(sysConfigService.removeConfig(id));
    }

}
