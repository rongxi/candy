package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.SysLogQueryParam;
import com.candy.biz.domain.param.SysLogSaveParam;
import com.candy.biz.domain.vo.OnlineUserVO;
import com.candy.biz.domain.vo.SysLogVO;
import com.candy.biz.service.SysLogService;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 系统在线用户-控制层。
 *
 * @author rong xi
 * @date 2023/10/11 16:59
 * @version 1.0.0
 */
@Tag(name = "系统在线用户")
@RestController
@RequestMapping("/sys-online-user")
@AllArgsConstructor
public class SysOnlineUserController {
    private final static String MODULE_NAME = "系统在线用户";
    private final SysLogService sysLogService;


    /**
     * 分页查询在线用户
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询在线用户")
    @SaCheckPermission("sys:online:list")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.QUERY,describe = "分页查询在线用户",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<OnlineUserVO>> queryOnlinePage(SysLogQueryParam param) {
        return Result.success(sysLogService.queryOnlinePage(param));
    }

    /**
     * 强退在线用户
     *
     * @param token 用户token
     * @return 强退结果
     */
    @Operation(summary = "强退",description = "强退在线用户")
    @Parameter(name = "token",description = "用户token",in = ParameterIn.PATH)
    @SaCheckPermission("sys:online:tickout")
    @OpLog(module = MODULE_NAME, action = LogActionEnum.OTHER)
    @PostMapping("tickout/{token}")
    public Result<Boolean> tickOutUser(@PathVariable("token") String token) {
        return Result.success(sysLogService.tickOutUser(token));
    }

}
