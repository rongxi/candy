package com.candy.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.candy.biz.domain.param.SysDeptQueryParam;
import com.candy.biz.domain.param.SysDeptSaveParam;
import com.candy.biz.domain.vo.SysDeptVO;
import com.candy.common.annotations.OpLog;
import com.candy.common.domain.PageVO;
import com.candy.common.domain.Result;
import com.candy.common.enums.LogActionEnum;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.candy.biz.service.SysDeptService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import cn.hutool.core.lang.tree.Tree;

/**
 * 部门-控制层。
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Tag(name = "部门")
@RestController
@RequestMapping("/sys-dept")
@AllArgsConstructor
public class SysDeptController {
    private final static String MODULE_NAME = "部门";
    private final SysDeptService sysDeptService;

    /**
     * 查询部门详情
     *
     * @param id 部门主键
     * @return 详情结果
     */
    @Operation(summary = "详情",description = "查询部门详情")
    @Parameter(name = "id",description = "部门id",in = ParameterIn.PATH)
    @SaCheckPermission("biz:dept:info")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.QUERY,describe = "查询部门详情")
    @GetMapping("info/{id}")
    public Result<SysDeptVO> queryInfo(@PathVariable("id") Long id) {
        return Result.success(sysDeptService.queryInfo(id));
    }

    /**
     * 分页查询部门
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Operation(summary = "分页查询",description = "分页查询部门")
    @SaCheckPermission("biz:dept:list")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.QUERY,describe = "分页查询部门",saveResponse = false)
    @GetMapping("page")
    public Result<PageVO<SysDeptVO>> queryPage(SysDeptQueryParam param) {
        return Result.success(sysDeptService.queryPage(param));
    }

    /**
     * 查询部门树列表
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @Operation(summary = "查询树列表",description = "查询部门树列表")
    @SaCheckPermission("biz:dept:list")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.QUERY,describe = "查询部门树列表",saveResponse = false)
    @GetMapping("tree-list")
    public Result<List<Tree<Long>>> queryTreeList(SysDeptQueryParam param) {
        return Result.success(sysDeptService.queryTreeList(param));
    }

    /**
     * 查询部门树
     *
     * @param param 查询参数
     * @return 查询结果
     */
    @Operation(summary = "查询树对象",description = "查询部门树")
    @SaCheckPermission("biz:dept:list")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.QUERY,describe = "查询部门树")
    @GetMapping("tree")
    public Result<Tree<Long>> queryTree(SysDeptQueryParam param) {
        return Result.success(sysDeptService.queryTree(param));
    }

    /**
    * 导出部门
    *
    * @param param 查询参数
    */
    @Operation(summary = "数据导出",description = "导出部门")
    @SaCheckPermission("biz:dept:export")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.EXPORT,saveResponse = false)
    @GetMapping("export")
    public void export(SysDeptQueryParam param) {
        sysDeptService.export(param);
    }

    /**
     * 保存部门
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Operation(summary = "保存",description = "新增或者修改部门")
    @SaCheckPermission(value = {"biz:dept:add", "biz:dept:edit"}, mode = SaMode.OR)
    @OpLog(module = MODULE_NAME,action = LogActionEnum.SAVE)
    @PostMapping(value="save",produces = MediaType.APPLICATION_JSON_VALUE)
    public Result<Boolean> saveHandle(@RequestBody @Validated SysDeptSaveParam param) {
        return Result.success(sysDeptService.saveHandle(param));
    }

    /**
     * 删除部门
     *
     * @param id 部门主键
     * @return 删除结果
     */
    @Operation(summary = "删除",description = "删除部门")
    @Parameter(name = "id",description = "部门id",in = ParameterIn.PATH)
    @SaCheckPermission("biz:dept:remove")
    @OpLog(module = MODULE_NAME,action = LogActionEnum.DELETE)
    @PostMapping("remove/{id}")
    public Result<Boolean> remove(@PathVariable("id") Long id) {
        return Result.success(sysDeptService.removeById(id));
    }

}
