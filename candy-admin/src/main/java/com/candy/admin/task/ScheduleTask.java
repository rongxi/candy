package com.candy.admin.task;

import com.candy.biz.service.ScheduleTaskService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;


/**
 * 定时任务类
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
//@Component
@Slf4j
@AllArgsConstructor
public class ScheduleTask {
    private final ScheduleTaskService scheduleTaskService;


    /**
     * 操作日志
     * 每次执行间隔1秒钟
     */
    @Async
    @Scheduled(fixedDelay = 1000)
    public void demoTask(){
        scheduleTaskService.demoTask();
    }
}
