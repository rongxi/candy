package com.candy;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * 启动类
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Slf4j
@SpringBootApplication
@MapperScan("com.candy.biz.mapper")
@Caching
@EnableConfigurationProperties
public class AdminApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AdminApplication.class);
    }

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(AdminApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = Optional.ofNullable(env.getProperty("server.servlet.context-path")).orElse("");
        log.info("\n-----------------------------------------------------------------------\n\t" +
                "candy-admin is running! access urls:\n\t" +
                "local: \t\thttp://localhost:" + port + path + "\n\t" +
                "external: \thttp://" + ip + ":" + port + path + "\n\t" +
                "接口文档: \thttp://" + ip + ":" + port + path + "doc.html\n" +
                "-----------------------------------------------------------------------");

    }

}