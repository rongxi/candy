package com.candy.codegen.constant;

import java.util.Arrays;
import java.util.List;

/**
 * 代码生成器常量
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
public interface GenConstants {

    Integer YES = 1;

    Integer NO = 0;
    /**
     * 单表（增删改查）
     */
    String TPL_CRUD = "crud";

    /**
     * 树表（增删改查）
     */
    String TPL_TREE = "tree";

    /**
     * 主子表（增删改查）
     */
    String TPL_SUB = "sub";

    /**
     * 树编码字段
     */
    String TREE_CODE = "treeCode";

    /**
     * 树父编码字段
     */
    String TREE_PARENT_CODE = "treeParentCode";

    /**
     * 树名称字段
     */
    String TREE_NAME = "treeName";

    /**
     * 上级菜单ID字段
     */
    String PARENT_MENU_ID = "parentMenuId";

    /**
     * 上级菜单名称字段
     */
    String PARENT_MENU_NAME = "parentMenuName";

    /**
     * 数据库字符串类型
     */
    String[] COLUMNTYPE_STR = {"char", "varchar", "nvarchar", "varchar2"};

    /**
     * 数据库文本类型
     */
    String[] COLUMNTYPE_TEXT = {"tinytext", "text", "mediumtext", "longtext"};

    /**
     * 数据库时间类型
     */
    String[] COLUMNTYPE_TIME = {"datetime", "time", "date", "timestamp"};

    /**
     * 数据库数字类型
     */
    String[] COLUMNTYPE_NUMBER = {"tinyint", "smallint", "mediumint", "int", "number", "integer",
            "bit", "bigint", "float", "double", "decimal"};

    /**
     * 页面不需要编辑字段
     */
    String[] COLUMNNAME_NOT_EDIT = {"id", "create_user", "create_time", "update_time", "update_user", "del_flag"};

    /**
     * 页面不需要显示的列表字段
     */
    String[] COLUMNNAME_NOT_LIST = {"id", "create_user", "create_time", "update_time", "update_user", "del_flag"};

    /**
     * 页面不需要查询字段
     */
    String[] COLUMNNAME_NOT_QUERY = {"id", "create_user", "create_time", "del_flag", "update_user",
            "update_time", "remark"};

    /**
     * Entity基类字段
     */
    String[] BASE_ENTITY = {"createUser", "createTime", "updateUser", "updateTime", "remark"};

    /**
     * Tree基类字段
     */
    String[] TREE_ENTITY = {"parentName", "parentId", "orderNum", "ancestors"};

    /**
     * 文本框
     */
    String HTML_INPUT = "input";

    /**
     * 文本域
     */
    String HTML_TEXTAREA = "textarea";

    /**
     * 下拉框
     */
    String HTML_SELECT = "select";

    /**
     * 单选框
     */
    String HTML_RADIO = "radio";

    /**
     * 复选框
     */
    String HTML_CHECKBOX = "checkbox";

    /**
     * 日期控件
     */
    String HTML_DATETIME = "datetime";

    /**
     * 上传控件
     */
    String HTML_UPLOAD = "upload";

    /**
     * 富文本控件
     */
    String HTML_SUMMERNOTE = "summernote";

    /**
     * 字符串类型
     */
    String TYPE_STRING = "String";

    /**
     * 整型
     */
    String TYPE_INTEGER = "Integer";

    /**
     * 长整型
     */
    String TYPE_LONG = "Long";

    /**
     * 浮点型
     */
    String TYPE_DOUBLE = "Double";

    /**
     * 高精度计算类型
     */
    String TYPE_BIG_DECIMAL = "BigDecimal";

    /**
     * 时间类型
     */
    String TYPE_DATE = "LocalDateTime";

    /**
     * boolean类型
     */
    String TYPE_BOOLEAN = "Boolean";

    /**
     * 模糊查询
     */
    String QUERY_LIKE = "like";

    /**
     * 相等查询
     */
    String QUERY_EQ = "eq";

    /**
     * 需要
     */
    Integer REQUIRE = 1;
    /**
     * 需要
     */
    Integer NOT_REQUIRE = 0;

    List<String> commonFields = Arrays.asList("id", "create_user", "create_time", "update_time", "update_user", "del_flag","order_num");

}
