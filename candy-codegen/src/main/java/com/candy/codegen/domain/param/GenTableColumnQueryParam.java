package com.candy.codegen.domain.param;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 代码生成表字段信息查询参数
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 17:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GenTableColumnQueryParam extends QueryBaseParam {

    /**
     * 表id
     */
    private Long id;

}
