package com.candy.codegen.domain.vo;

import lombok.Data;
import java.time.LocalDateTime;

/**
 * 代码生成表字段信息
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Data
public class GenTableColumnVO{

    /**
     * 编号
     */
    private Long id;

    /**
     * 归属表编号
     */
    private Long tableId;

    /**
     * 归属表名称
     */
    private String tableName;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列名称大写
     */
    private String columnNameUpper;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * JAVA类型
     */
    private String javaType;

    /**
     * JAVA字段名
     */
    private String javaField;

    /**
     * 是否主键（1是）
     */
    private Integer isPk;

    /**
     * 是否自增（1是）
     */
    private Integer isIncrement;

    /**
     * 是否必填（1是）
     */
    private Integer isRequired;

    /**
     * 是否为插入字段（1是）
     */
    private Integer isInsert;

    /**
     * 是否编辑字段（1是）
     */
    private Integer isEdit;

    /**
     * 是否列表字段（1是）
     */
    private Integer isList;

    /**
     * 是否查询字段（1是）
     */
    private Integer isQuery;

    /**
     * 是否需要验证
     */
    private Integer isVerify;

    /**
     * 查询方式（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）
     */
    private String queryType;

    /**
     * 显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件、upload上传控件、summernote富文本控件）
     */
    private String htmlType;

    /**
     * 字典类型
     */
    private String dictType = "";

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 创建者
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * get方法
     */
    private String getMethod;
    /**
     * set方法
     */
    private String setMethod;

    /**
     * 内容长度
     */
    private Integer contentLength;

}