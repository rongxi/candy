package com.candy.codegen.domain.param;


import cn.hutool.core.util.StrUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.query.QueryOrderBy;
import com.mybatisflex.core.table.TableDef;
import lombok.Data;
import java.util.Optional;

/**
 * 通用查询参数
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Data
public class QueryBaseParam {

    private String sortField;

    private String sortType;

    private Integer pageNum;

    private Integer pageSize;

    public <T> Page<T> getPage(){
        pageNum = Optional.ofNullable(pageNum).filter(s -> s >= 1).orElse(1);
        pageSize = Optional.ofNullable(pageSize).filter(s -> s >= 1).orElse(10);
        return new Page<>(pageNum,pageSize);
    }

    public <K extends TableDef> QueryOrderBy getOrderBy(K tableDef){
        sortField = StrUtil.isNotBlank(sortField)?StrUtil.toUnderlineCase(sortField):"create_time";
        sortType = StrUtil.isNotBlank(sortType)?sortType.toUpperCase():"DESC";
        return new QueryOrderBy( new QueryColumn(tableDef, sortField),sortType);
    }

}
