package com.candy.codegen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * AJAX结果对象
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class AjaxResult<T> {

    /**
     * 状态码
     */
    public int code;

    /**
     * 返回内容
     */
    public String msg;

    /**
     * 数据对象
     */
    public T data;


    /**
     * 返回成功消息
     * @return 成功消息
     */
    public static AjaxResult<?> success() {
        return new AjaxResult<>(0,"操作成功",null);
    }

    /**
     * 返回成功数据
     * @return 成功消息
     */
    public  static <T> AjaxResult<T> success(T data) {
        return new AjaxResult<>(0,"操作成功", data);
    }

    /**
     * 返回成功消息
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult<?> success(String msg) {
        return new AjaxResult<>(0,msg, null);
    }

    /**
     * 返回成功消息
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> AjaxResult<T> success(String msg, T data) {
        return new AjaxResult<>(0, msg, data);
    }


    /**
     * 返回成功消息
     * @return 成功消息
     */
    public static AjaxResult<?> error() {
        return new AjaxResult<>(500,"服务器异常",null);
    }

    /**
     * 返回成功数据
     * @return 成功消息
     */
    public  static <T> AjaxResult<T> error(T data) {
        return new AjaxResult<>(500,"服务器异常", data);
    }

    /**
     * 返回成功消息
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult<?> error(String msg) {
        return new AjaxResult<>(500,msg, null);
    }

    /**
     * 返回成功消息
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> AjaxResult<T> error(String msg, T data) {
        return new AjaxResult<>(500, msg, data);
    }

    /**
     * 返回成功消息
     * @return 成功消息
     */
    public static AjaxResult<?> warn() {
        return new AjaxResult<>(301,"参数错误",null);
    }

    /**
     * 返回成功数据
     * @return 成功消息
     */
    public  static <T> AjaxResult<T> warn(T data) {
        return new AjaxResult<>(500, "参数错误", data);
    }

    /**
     * 返回成功消息
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult<?> warn(String msg) {
        return new AjaxResult<>(301,msg, null);
    }

    /**
     * 返回成功消息
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> AjaxResult<T> warn(String msg, T data) {
        return new AjaxResult<>(301, msg, data);
    }

}
