package com.candy.codegen.domain;

import com.mybatisflex.core.paginate.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 分页查询结果
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class PageResult<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 总记录数
     */
    private long total;

    /**
     * 列表数据
     */
    private List<T> rows;

    /**
     * 消息状态码
     */
    private int code;

    /**
     * 消息内容
     */
    private String msg;


    public static <T> PageResult<T> of(@NonNull Page<T> page){
        return new PageResult<>(page.getTotalRow(), page.getRecords(), 0, "成功");
    }

    public static <T> PageResult<T> of(long total, List<T> rows){
        return new PageResult<>(total, rows, 0, "");
    }


}