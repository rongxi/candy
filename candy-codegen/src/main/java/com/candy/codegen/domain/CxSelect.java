package com.candy.codegen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;


/**
 * CxSelect树结构实体类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CxSelect{

    /**
     * 数据值字段名称
     */
    private String v;

    /**
     * 数据标题字段名称
     */
    private String n;

    /**
     * 子集数据字段名称
     */
    private List<CxSelect> s;

}
