package com.candy.codegen;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * 单体启动类
 * 报错提醒: 未集成mongo报错，可以打开启动类上面的注释 exclude={MongoAutoConfiguration.class}
 *
 * @author rong'xi
 */
@Slf4j
@SpringBootApplication
@MapperScan("com.candy.codegen.mapper")
public class CodegenApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CodegenApplication.class);
    }

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(CodegenApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = Optional.ofNullable(env.getProperty("server.servlet.context-path")).orElse("");
        log.info("\n----------------------------------------------------------\n\t" +
                "candy-codegen is running! access urls:\n\t" +
                "local: \t\thttp://localhost:" + port + path + "/gen\n\t" +
                "external: \thttp://" + ip + ":" + port + path + "/gen\n\t" +
                "----------------------------------------------------------");

    }

}