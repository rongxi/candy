package com.candy.codegen.util;


import cn.hutool.core.util.CharsetUtil;
import org.apache.velocity.app.Velocity;
import java.util.Properties;

/**
 * VelocityEngine工厂
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
public class VelocityInitializer {
    /**
     * 初始化vm方法
     */
    public static void initVelocity() {
        Properties p = new Properties();
        try {
            // 加载classpath目录下的vm文件
            p.setProperty("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            // 定义字符集
            p.setProperty(Velocity.INPUT_ENCODING, CharsetUtil.UTF_8);
            // 初始化Velocity引擎，指定配置Properties
            Velocity.init(p);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
