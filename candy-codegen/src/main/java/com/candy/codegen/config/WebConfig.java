package com.candy.codegen.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * web配置类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    private static final String DEFAULT_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    @Primary
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customJackson() {
        return jacksonObjectMapperBuilder -> {
            jacksonObjectMapperBuilder.serializationInclusion(JsonInclude.Include.NON_NULL);
            jacksonObjectMapperBuilder.failOnUnknownProperties(false);
            // jacksonObjectMapperBuilder.propertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
            jacksonObjectMapperBuilder.failOnEmptyBeans(false);
            //jacksonObjectMapperBuilder.featuresToEnable(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS,JsonReadFeature.ALLOW_SINGLE_QUOTES);
            jacksonObjectMapperBuilder.dateFormat(new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN));
            DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_PATTERN);
            jacksonObjectMapperBuilder.serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(localDateTimeFormatter));
            jacksonObjectMapperBuilder.deserializerByType(LocalDateTime.class, new LocalDateTimeDeserializer(localDateTimeFormatter));
            DateTimeFormatter localDateFormatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_PATTERN);
            jacksonObjectMapperBuilder.serializerByType(LocalDate.class, new LocalDateSerializer(localDateFormatter));
            jacksonObjectMapperBuilder.deserializerByType(LocalDate.class, new LocalDateDeserializer(localDateFormatter));
            jacksonObjectMapperBuilder.serializerByType(BigDecimal.class, new BigDecimalSerializer());
            jacksonObjectMapperBuilder.deserializerByType(Date.class, new DateDeserializer());
            jacksonObjectMapperBuilder.serializerByType(Long.TYPE, ToStringSerializer.instance);
            jacksonObjectMapperBuilder.serializerByType(Long.class, ToStringSerializer.instance);
        };
    }

    /**
     * BigDecimal序列化实现
     */
    public static class BigDecimalSerializer extends JsonSerializer<BigDecimal> {
        @Override
        public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            if (value != null) {
                gen.writeString(value.stripTrailingZeros().toPlainString());
            }
        }
    }


    /**
     * Date序列化实现
     */
    public static class DateDeserializer extends JsonDeserializer<Date>{

        @Override
        public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            String originDate = jsonParser.getText();
            if (StrUtil.isNotEmpty(originDate)) {
                originDate = originDate.trim();
                if(originDate.length() == 10){
                    return DateUtil.parse(originDate,DEFAULT_DATE_PATTERN);
                }
                return DateUtil.parse(originDate,DEFAULT_DATE_TIME_PATTERN);
            }
            return null;
        }
    }

}
