package com.candy.codegen.config;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 代码生成器配置类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Component
@ConfigurationProperties(prefix = "gen")
@Data
public class GenConfig {

    /**
     * 保存实例到静态变量中方便使用
     */
    public static GenConfig instance;

    /**
     * 作者
     */
    public String author;

    /**
     * 生成包路径
     */
    public String packageName;

    /**
     * 自动去除表前缀，默认是false
     */
    public Boolean autoRemovePre;

    /**
     * 表前缀(类名不会包含表前缀)
     */
    public String tablePrefix;

    /**
     * 初始化方法
     */
    @PostConstruct
    private void init(){
        instance = this;
    }

}
