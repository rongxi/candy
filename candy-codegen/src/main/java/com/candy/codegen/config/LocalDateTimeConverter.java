package com.candy.codegen.config;

import cn.hutool.core.util.StrUtil;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * POST/GET application/x-www-form-urlencoded 传参日期类型转换类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Component
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {
    private static final  String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Override
    @SuppressWarnings("all")
    public LocalDateTime convert(String source) {
        if(StrUtil.isBlank(source)){
            return null;
        }

        if(source.length() == 10){
            return LocalDateTime.parse(source+" 00:00:00", DateTimeFormatter.ofPattern(DEFAULT_DATETIME_PATTERN));
        }
        return LocalDateTime.parse(source, DateTimeFormatter.ofPattern(DEFAULT_DATETIME_PATTERN));
    }
}
