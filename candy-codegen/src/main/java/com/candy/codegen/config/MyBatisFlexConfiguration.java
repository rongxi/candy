package com.candy.codegen.config;

import com.mybatisflex.core.audit.AuditManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisFlex配置类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Configuration
public class MyBatisFlexConfiguration {

    private static final Logger logger = LoggerFactory
        .getLogger("mybatis-flex-sql");


    public MyBatisFlexConfiguration() {
        //开启审计功能
        AuditManager.setAuditEnable(true);

        //设置 SQL 审计收集器
        AuditManager.setMessageCollector(auditMessage ->
            logger.info("{},{}ms", auditMessage.getFullSql()
                , auditMessage.getElapsedTime())
        );
    }
}