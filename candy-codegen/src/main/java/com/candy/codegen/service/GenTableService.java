package com.candy.codegen.service;


import com.candy.codegen.domain.entity.GenTable;
import com.candy.codegen.domain.PageResult;
import com.candy.codegen.domain.param.GenTableQueryParam;
import com.candy.codegen.domain.param.GenTableSaveParam;
import com.candy.codegen.domain.vo.GenTableVO;
import com.mybatisflex.core.service.IService;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

/**
 * 代码生成表服务接口
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
public interface GenTableService extends IService<GenTable> {

    /**
     * 分页查询代码生成表列表
     * @param param 查询条件
     * @return 分页对象
     */
    PageResult<GenTableVO> page(GenTableQueryParam param);

    /**
     * 分页查询数据库表列表
     * @param param 查询条件
     * @return 分页对象
     */
    PageResult<GenTableVO> pageDb(GenTableQueryParam param);

    /**
     * 查询所有的代码生成表列表
     * @return 表VO列表
     */
    List<GenTableVO> listAllVo();

    /**
     * 根据id查询代码生成表VO
     * @param id 表id
     * @return 表VO对象
     */
    GenTableVO getVoById(Long id);

    /**
     * 根据表名称查询代码生成表VO
     * @param tableName 表名称
     * @return 表VO对象
     */
    GenTableVO getVoByTableName(String tableName);

    /**
     * 更新代码生成表
     * @param param 代码生成表名称
     */
    void modify(GenTableSaveParam param);

    /**
     * 根据ids删除代码生成表
     * @param ids 表ids
     */
    void delete(String ids);

    /**
     * 导入代码生成表
     * @param tableList 生成表列表
     */
    void importGenTable(List<GenTable> tableList);

    /**
     * 根据id生成预览代码
     * @param id 表id
     * @return 代码map
     */
    Map<String, String> previewCode(Long id);

    /**
     * 根据id自定义路径生成代码
     * @param id 表id
     */
    void generatorCode(Long id);

    /**
     * 根据ids批量生成代码
     * @param ids 代码生成表ids
     * @return 代码字节数组
     */
    byte[] batchDownloadCode(String ids);

    /**
     * 根据表名称导入表信息
     * @param tableNames 表名称
     */
    void importGenTable(String tableNames);

    /**
     * 根据表id查询表更新信息
     * @param id 表id
     * @param modelMap 视图map对象
     */
    void getEditInfo(Long id, ModelMap modelMap);

}
