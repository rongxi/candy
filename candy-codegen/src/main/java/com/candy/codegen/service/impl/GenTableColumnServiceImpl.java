package com.candy.codegen.service.impl;


import com.candy.codegen.domain.entity.GenTableColumn;
import com.candy.codegen.domain.PageResult;
import com.candy.codegen.mapper.GenTableColumnMapper;
import com.candy.codegen.domain.param.GenTableColumnQueryParam;
import com.candy.codegen.service.GenTableColumnService;
import com.candy.codegen.domain.vo.GenTableColumnVO;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.candy.codegen.domain.entity.table.GenTableColumnTableDef.GEN_TABLE_COLUMN;


/**
 * 代码生成表字段服务类
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Service
@Slf4j
public class GenTableColumnServiceImpl extends ServiceImpl<GenTableColumnMapper, GenTableColumn>  implements GenTableColumnService {

    /**
     * 分页查询表字段
     * @param param 查询条件
     * @return 分页结果
     */
    @Override
    public PageResult<GenTableColumnVO> getTableColumnData(GenTableColumnQueryParam param) {
        Page<GenTableColumnVO> page = queryChain()
                .select(GEN_TABLE_COLUMN.ALL_COLUMNS)
                .from(GEN_TABLE_COLUMN)
                .where(GEN_TABLE_COLUMN.TABLE_ID.eq(param.getId()))
                .orderBy(param.getOrderBy(GEN_TABLE_COLUMN))
                .pageAs(param.getPage(),GenTableColumnVO.class);
        return PageResult.of(page);
    }

}