package com.candy.codegen.service;

import com.candy.codegen.domain.PageResult;
import com.candy.codegen.domain.entity.GenTableColumn;
import com.candy.codegen.domain.param.GenTableColumnQueryParam;
import com.candy.codegen.domain.vo.GenTableColumnVO;
import com.mybatisflex.core.service.IService;

/**
 * 代码生成表字段服务接口
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
public interface GenTableColumnService extends IService<GenTableColumn> {

    /**
     * 分页查询表字段
     * @param param 查询条件
     * @return 分页结果
     */
    PageResult<GenTableColumnVO> getTableColumnData(GenTableColumnQueryParam param);



}
