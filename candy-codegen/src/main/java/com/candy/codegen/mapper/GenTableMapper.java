package com.candy.codegen.mapper;


import com.candy.codegen.domain.entity.GenTable;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 * 代码生成表mapper
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 23:44
 */
@Mapper
public interface GenTableMapper extends BaseMapper<GenTable> {

}