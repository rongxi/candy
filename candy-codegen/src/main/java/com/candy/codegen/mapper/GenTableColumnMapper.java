package com.candy.codegen.mapper;


import com.candy.codegen.domain.entity.GenTableColumn;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 代码生成表字段mapper
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 23:44
 */
@Mapper
public interface GenTableColumnMapper extends BaseMapper<GenTableColumn> {

    List<GenTableColumn> selectDbTableColumnsByNames(@Param("tableNames") List<String> tableNames);

}
