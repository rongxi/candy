package ${packageName}.domain.vo;

import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * ${functionName}-视图对象
 *
 * @author ${author}
 * @date ${datetime}
 * @version 1.0.0
 */
@Data
@Schema(name = "${functionName}-视图对象",description = "${functionName}-视图对象")
public class ${ClassName}VO implements Serializable {
    #foreach ($column in $columns)
        #if($column.isList)
        #set($parentheseIndex=$column.columnComment.indexOf("("))
        #if($parentheseIndex != -1)
            #set($comment=$column.columnComment.substring(0, $parentheseIndex))
        #else
            #set($comment=$column.columnComment)
        #end
    /**
     * $column.columnComment
     */
    @ExcelProperty("${comment}")
    @Schema(description = "$column.columnComment")
    #if($column.dictType)
    @Dict("$column.dictType")
    #end
    private $column.javaType $column.javaField;
        #end
    #end

    #if($table.tree)
    /**
     * 下级
     */
    private List<${ClassName}VO> children;
    #end
}
