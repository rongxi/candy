package com.candy.biz.domain.vo;

import com.candy.common.annotations.Dict;
import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 操作日志-视图对象
 *
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
@Data
@Schema(name = "操作日志-视图对象",description = "操作日志-视图对象")
public class SysLogVO implements Serializable {
    /**
     * id
     */
    @ExcelProperty("id")
    @Schema(description = "id")
    private Long id;
    /**
     * 模块
     */
    @ExcelProperty("模块")
    @Schema(description = "模块")
    private String moduleName;
    /**
     * 操作类型(登录,退出,新增,修改,保存,删除,其他)
     */
    @ExcelProperty("操作类型")
    @Schema(description = "操作类型(登录,退出,新增,修改,保存,删除,其他)")
    @Dict("log_type")
    private String type;
    /**
     * 描述
     */
    @ExcelProperty("描述")
    @Schema(description = "描述")
    private String describe;
    /**
     * 用户id
     */
    @ExcelProperty("用户id")
    @Schema(description = "用户id")
    private Long userId;
    /**
     * 用户账号
     */
    @ExcelProperty("用户账号")
    @Schema(description = "用户账号")
    private String userAccount;
    /**
     * 用户名称
     */
    @ExcelProperty("用户名称")
    @Schema(description = "用户名称")
    private String userName;
    /**
     * 请求ip
     */
    @ExcelProperty("请求ip")
    @Schema(description = "请求ip")
    private String requestIp;
    /**
     * ip归属地
     */
    @ExcelProperty("ip归属地")
    @Schema(description = "ip归属地")
    private String requestRegion;
    /**
     * 请求方式
     */
    @ExcelProperty("请求方式")
    @Schema(description = "请求方式")
    private String requestMethod;
    /**
     * 请求参数
     */
    @ExcelProperty("请求参数")
    @Schema(description = "请求参数")
    private String requestParam;
    /**
     * 请求url
     */
    @ExcelProperty("请求url")
    @Schema(description = "请求url")
    private String requestUrl;
    /**
     * 请求token
     */
    @ExcelProperty("请求token")
    @Schema(description = "请求token")
    private String requestToken;
    /**
     * 浏览器
     */
    @ExcelProperty("浏览器")
    @Schema(description = "浏览器")
    private String clientBrowser;
    /**
     * 操作系统
     */
    @ExcelProperty("操作系统")
    @Schema(description = "操作系统")
    private String clientOs;
    /**
     * 请求时间
     */
    @ExcelProperty("请求时间")
    @Schema(description = "请求时间")
    private LocalDateTime requestTime;
    /**
     * 耗时(毫秒)
     */
    @ExcelProperty("耗时")
    @Schema(description = "耗时(毫秒)")
    private Long costTime;
    /**
     * 错误信息
     */
    @ExcelProperty("错误信息")
    @Schema(description = "错误信息")
    private String errorMsg;
    /**
     * 返回内容
     */
    @ExcelProperty("返回内容")
    @Schema(description = "返回内容")
    private String responseContent;
    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
