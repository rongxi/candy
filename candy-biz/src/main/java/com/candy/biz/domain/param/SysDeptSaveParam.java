package com.candy.biz.domain.param;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 部门-保存参数
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Data
@Schema(name = "部门-保存参数",description = "部门-保存参数")
public class SysDeptSaveParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;

    /**
     * 部门名称
     */
    @Schema(description = "部门名称",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "部门名称不能为空")
    @Size(min = 1,max = 50,message = "部门名称长度在1和50之间")
    private String deptName;

    /**
     * 上级部门
     */
    @Schema(description = "上级部门",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "上级部门不能为空")
    private Long parentId;



    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;





}
