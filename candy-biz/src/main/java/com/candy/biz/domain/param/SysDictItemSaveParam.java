package com.candy.biz.domain.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;
import java.util.List;

/**
 * 字典项-保存参数
 * @author rong xi
 * @version 1.0
 * @date 2023/09/25 14:28
 */
@Data
@Schema(name = "字典项-保存参数",description = "字典项-保存参数")
public class SysDictItemSaveParam {

    /**
     * 字典id
     */
    @Schema(description = "字典id")
    @NotNull(message = "字典id不能为空")
    private Long dictId;

    /**
     * 字典编码
     */
    @Schema(description = "字典编码")
    @NotEmpty(message = "字典编码不能为空")
    private String dictCode;

    /**
     * 待保存字典项
     */
    @Schema(description = "待保存字典项")
    List<SysDictItemParam> items;
}
