package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;

/**
 * 系统角色-查询参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "系统角色-查询参数",description = "系统角色-查询参数")
public class SysRoleQueryParam extends QueryBaseParam implements Serializable {

    /**
     * id
     */
    @Schema(description = "角色ID")
    private Long id;

    /**
     * 角色标识
     */
    @Schema(description = "角色标识")
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(description = "角色名称")
    private String roleName;
}
