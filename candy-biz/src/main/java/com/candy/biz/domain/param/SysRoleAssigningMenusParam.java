package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 系统角色-分配菜单参数
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统角色-分配菜单参数",description = "系统角色-分配菜单参数")
public class SysRoleAssigningMenusParam implements Serializable {

    @Schema(description = "角色id")
    @NotNull(message = "角色id不能为空")
    private Long roleId;

    /**
     * 菜单IDS
     */
    @Schema(description = "菜单ids")
    private List<Long> menuIds;

}
