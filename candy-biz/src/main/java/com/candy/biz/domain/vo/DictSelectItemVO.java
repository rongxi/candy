package com.candy.biz.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 字典下拉选择
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/18 17:05
 */
@Data
@Builder
@AllArgsConstructor
public class DictSelectItemVO {
    /**
     * 字典名
     */
    private String label;

    /**
     * 字典值
     */
    private String value;
}
