package com.candy.biz.domain.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;


/**
 * 字典-保存参数
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/25 14:27
 */
@Schema(name = "字典-保存参数",description = "字典-保存参数")
@Data
public class SysDictSaveParam {

    /**
     * 字段id
     */
    @Schema(description = "字典ID")
    private Long id;

    /**
     * 字典标识
     */
    @Schema(description = "字典标识")
    @NotEmpty(message = "字典标识不能为空")
    @Size(min = 1,max = 30,message = "字典标识字符长度在1到30")
    @Pattern(regexp = "^[a-z][a-z0-9_]*$", message = "字典类型必须以字母开头，且只能为（小写字母，数字，下滑线）")
    private String dictCode;

    /**
     * 字典名称
     */
    @Schema(description = "字典名称")
    @NotEmpty(message = "字典名称不能为空")
    @Size(min = 1,max = 30,message = "字典名称字符长度在1到30")
    private String dictName;

    /**
     * 备注
     */
    private String remark;

}
