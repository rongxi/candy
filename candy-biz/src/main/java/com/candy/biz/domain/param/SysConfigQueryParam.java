package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 参数配置-查询参数
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "参数配置-查询参数",description = "参数配置-查询参数")
public class SysConfigQueryParam extends QueryBaseParam implements Serializable {
    /**
     * 参数主键
     */
    @Schema(description = "参数主键")
    private Long id;
    /**
     * 参数名称
     */
    @Schema(description = "参数名称")
    private String configName;
    /**
     * 参数键名
     */
    @Schema(description = "参数键名")
    private String configKey;
    /**
     * 类型(0系统参数 1普通参数)
     */
    @Schema(description = "类型(0系统参数 1普通参数)")
    private Integer type;
}
