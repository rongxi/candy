package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.io.Serializable;


/**
 * 系统用户-重置登录密码参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-重置登录密码参数",description = "系统用户-重置登录密码参数")
public class SysUserResetPasswordParam implements Serializable {

    /**
     * 用户ID
     */
    @NotNull(message = "用户id不能为空")
    @Schema(description = "用户ID")
    private Long id;

    /**
     * 登录密码
     */
    @NotEmpty(message = "登录密码不能为空")
    @Size(min = 6, max = 20, message = "登录密码长度在6-20之间")
    @Schema(description = "登录密码")
    private String loginPassword;




}
