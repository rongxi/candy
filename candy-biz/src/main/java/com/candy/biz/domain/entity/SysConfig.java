package com.candy.biz.domain.entity;


import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


/**
 * 参数配置-实体类
 * 
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "sys_config", schema = "candy")
public class SysConfig implements Serializable {

    /**
     * 参数主键
     */
    @Id
    private Long id;

    /**
     * 参数名称
     */
    private String configName;

    /**
     * 参数键名
     */
    private String configKey;

    /**
     * 参数键值
     */
    private String configValue;

    /**
     * 类型(0系统参数 1普通参数)
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 删除标志(0代表存在 1代表删除)
     */
    private Boolean delFlag;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
