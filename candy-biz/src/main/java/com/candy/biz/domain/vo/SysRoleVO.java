package com.candy.biz.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.candy.biz.enums.RoleTypeEnum;
import com.candy.common.annotations.Dict;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统角色-视图对象。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统角色-视图对象",description = "系统角色-视图对象")
public class SysRoleVO implements Serializable {

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Long id;

    /**
     * 角色标识
     */
    @ExcelProperty("角色标识")
    @Schema(description = "角色标识")
    private String roleCode;

    /**
     * 角色名称
     */
    @ExcelProperty("角色名称")
    @Schema(description = "角色名称")
    private String roleName;

    /**
     * 类型(0系统角色 1普通角色)
     */
    @ExcelProperty("类型")
    @Schema(description = "类型(0系统角色 1普通角色)")
    @Dict(dictEnum = RoleTypeEnum.class)
    private Integer type;
    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @ExcelProperty("创建人")
    @Schema(description = "创建人")
    private Long createUser;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;


}
