package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;


/**
 * 字典项-参数
 * @author rong xi
 * @version 1.0
 * @date 2023/09/25 14:28
 */
@Data
@Schema(name = "字典项-参数",description = "字典项-参数")
public class SysDictItemParam {

    /**
     * 字典项名称
     */
    @Schema(description = "字典项名称")
    @NotEmpty(message = "字典项键不能为空")
    @Size(min = 1,max = 30,message = "字典项键字符长度在1到30")
    private String itemName;

    /**
     * 字典项值
     */
    @Schema(description = "字典项值")
    @NotEmpty(message = "字典项值不能为空")
    @Size(min = 1,max = 30,message = "字典项值字符长度在1到30")
    private String itemValue;

    /**
     * 状态（0正常 1停用）
     */
    @Schema(description = "状态（0正常 1停用）")
    @Min(value = 0,message = "状态不正确")
    @Max(value = 1,message = "状态不正确")
    private Integer state;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

}
