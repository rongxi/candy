package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;

/**
 * 系统用户查询条件。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "系统用户-查询参数",description = "系统用户-查询参数")
public class SysUserQueryParam extends QueryBaseParam implements Serializable {

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Long id;

    /**
     * 登录账号
     */
    @Schema(description = "登录账号")
    private String userAccount;

    /**
     * 用户名称
     */
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Schema(description = "帐号状态（0正常 1停用）")
    private Integer state;

    @Schema(description = "所属部门")
    private Long deptId;

}
