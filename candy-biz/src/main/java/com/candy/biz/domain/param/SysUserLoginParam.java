package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import java.io.Serializable;


/**
 * 系统用户-登录参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-登录参数",description = "系统用户-登录参数")
public class SysUserLoginParam implements Serializable {

    /**
     * 用户ID
     */
    @NotNull(message = "用户名不能为空")
    @Schema(description = "用户名")
    private String loginAccount;

    /**
     * 登录密码
     */
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "登录密码")
    private String loginPassword;

    /**
     * 验证码
     */
    @Schema(description = "验证码")
    private String captchaCode;

    /**
     * 客户端id
     */
    @Schema(description = "客户端id")
    private String clientId;

}
