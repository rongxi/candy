package com.candy.biz.domain.vo;

import com.candy.biz.enums.DictTypeEnum;
import com.candy.common.annotations.Dict;
import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 字典-视图对象
 *
 * @author rong xi
 * @date 2023/10/08 21:17
 * @version 1.0.0
 */
@Data
@Schema(name = "字典-视图对象",description = "字典-视图对象")
public class SysDictVO implements Serializable {
    /**
     * id
     */
    @ExcelProperty("id")
    @Schema(description = "id")
    private Long id;
    /**
     * 字典编号
     */
    @ExcelProperty("字典编号")
    @Schema(description = "字典编号")
    private String dictCode;
    /**
     * 字典名称
     */
    @ExcelProperty("字典名称")
    @Schema(description = "字典名称")
    private String dictName;
    /**
     * 类型(0系统字典 1普通字典)
     */
    @ExcelProperty("类型")
    @Schema(description = "类型(0系统字典 1普通字典)")
    @Dict(dictEnum = DictTypeEnum.class)
    private Integer type;
    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @ExcelProperty("创建人")
    @Schema(description = "创建人")
    private Long createUser;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
