package com.candy.biz.domain.param;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 菜单权限-保存参数
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Data
@Schema(name = "菜单权限-保存参数",description = "菜单权限-保存参数")
public class SysMenuSaveParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;

    /**
     * 名称
     */
    @Schema(description = "名称",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "名称不能为空")
    @Size(min = 1,max = 30,message = "名称长度在1和30之间")
    private String menuName;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 父级id
     */
    @Schema(description = "父级id",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "父级id不能为空")
    private Long parentId;

    /**
     * 类型(1菜单2权限)
     */
    @Schema(description = "类型(1菜单2权限)",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "类型不能为空")
    private Integer type;

    /**
     * 路由地址
     */
    @Schema(description = "路由地址")
    private String routePath;

    /**
     * 组件路径
     */
    @Schema(description = "组件路径")
    private String componentPath;

    /**
     * 权限标识
     */
    @Schema(description = "权限标识")
    private String permissionCode;

    /**
     * 类型(1菜单2权限)
     */
    @Schema(description = "顺序号")
    private Integer orderNum;

    /**
     * 备注
     */
    @Schema(description = "备注")
    @Size(min = 1,max = 100,message = "备注长度在1和100之间")
    private String remark;





}
