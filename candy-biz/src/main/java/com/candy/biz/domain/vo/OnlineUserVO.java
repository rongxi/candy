package com.candy.biz.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.candy.common.annotations.Dict;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 在线用户-视图对象
 *
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
@Data
@Schema(name = "在线用户-视图对象",description = "在线用户-视图对象")
public class OnlineUserVO implements Serializable {

    /**
     * 用户id
     */
    @ExcelProperty("用户id")
    @Schema(description = "用户id")
    private Long userId;
    /**
     * 用户账号
     */
    @ExcelProperty("用户账号")
    @Schema(description = "用户账号")
    private String userAccount;
    /**
     * 用户名称
     */
    @ExcelProperty("用户名称")
    @Schema(description = "用户名称")
    private String userName;
    /**
     * 请求ip
     */
    @ExcelProperty("请求ip")
    @Schema(description = "请求ip")
    private String requestIp;
    /**
     * ip归属地
     */
    @ExcelProperty("ip归属地")
    @Schema(description = "ip归属地")
    private String requestRegion;

    /**
     * 请求token
     */
    @ExcelProperty("请求token")
    @Schema(description = "请求token")
    private String requestToken;
    /**
     * 浏览器
     */
    @ExcelProperty("浏览器")
    @Schema(description = "浏览器")
    private String clientBrowser;
    /**
     * 操作系统
     */
    @ExcelProperty("操作系统")
    @Schema(description = "操作系统")
    private String clientOs;

    /**
     * 创建时间
     */
    @ExcelProperty("最后访问时间")
    @Schema(description = "最后访问时间")
    private LocalDateTime requestTime;

}
