package com.candy.biz.domain.vo;

import com.candy.biz.enums.ConfigTypeEnum;
import com.candy.common.annotations.Dict;
import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 参数配置-视图对象
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Data
@Schema(name = "参数配置-视图对象",description = "参数配置-视图对象")
public class SysConfigVO implements Serializable {
    /**
     * 参数主键
     */
    @ExcelProperty("参数主键")
    @Schema(description = "参数主键")
    private Long id;
    /**
     * 参数名称
     */
    @ExcelProperty("参数名称")
    @Schema(description = "参数名称")
    private String configName;
    /**
     * 参数键名
     */
    @ExcelProperty("参数键名")
    @Schema(description = "参数键名")
    private String configKey;
    /**
     * 参数键值
     */
    @ExcelProperty("参数键值")
    @Schema(description = "参数键值")
    private String configValue;
    /**
     * 类型(0系统参数 1普通参数)
     */
    @ExcelProperty("类型")
    @Schema(description = "类型(0系统参数 1普通参数)")
    @Dict(dictEnum = ConfigTypeEnum.class)
    private Integer type;
    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @ExcelProperty("创建人")
    @Schema(description = "创建人")
    private Long createUser;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
