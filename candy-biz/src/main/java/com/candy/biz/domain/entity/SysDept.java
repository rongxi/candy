package com.candy.biz.domain.entity;


import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


/**
 * 部门-实体类
 * 
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "sys_dept", schema = "candy")
public class SysDept implements Serializable {

    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 上级部门
     */
    private Long parentId;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 删除标志(0正常 1已删除)
     */
    private Boolean delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
