package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 菜单权限-查询参数
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "菜单权限-查询参数",description = "菜单权限-查询参数")
public class SysMenuQueryParam extends QueryBaseParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;
    /**
     * 名称
     */
    @Schema(description = "名称")
    private String menuName;
    /**
     * 类型(1菜单2权限)
     */
    @Schema(description = "类型(1菜单2权限)")
    private Integer type;
    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;
}
