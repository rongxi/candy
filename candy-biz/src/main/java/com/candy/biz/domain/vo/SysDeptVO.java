package com.candy.biz.domain.vo;

import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 部门-视图对象
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Data
@Schema(name = "部门-视图对象",description = "部门-视图对象")
public class SysDeptVO implements Serializable {
    /**
     * id
     */
    @ExcelProperty("id")
    @Schema(description = "id")
    private Long id;
    /**
     * 部门名称
     */
    @ExcelProperty("部门名称")
    @Schema(description = "部门名称")
    private String deptName;
    /**
     * 上级部门
     */
    @ExcelProperty("上级部门id")
    @Schema(description = "上级部门id")
    private Long parentId;

    /**
     * 上级部门
     */
    @ExcelProperty("上级部门")
    @Schema(description = "上级部门")
    private String parentName;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;
    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
