package com.candy.biz.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.candy.biz.enums.SexEnum;
import com.candy.biz.enums.StateEnum;
import com.candy.biz.enums.UserTypeEnum;
import com.candy.common.annotations.Dict;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户-视图类。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-视图类",description = "系统用户-视图类")
public class SysUserVO implements Serializable {

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Long id;

    /**
     * 用户名称
     */
    @ExcelProperty("用户名称")
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 登录账号
     */
    @ExcelProperty("登录账号")
    @Schema(description = "登录账号")
    private String loginAccount;

    /**
     * 用户类型（0超管用户 1普通用户）
     */
    @Schema(description = "用户类型（0超管用户 1普通用户）")
    @Dict(dictEnum = UserTypeEnum.class)
    private Integer type;

    @ExcelProperty("用户类型名称")
    @Schema(description = "用户类型名称")
    private String typeName;

    /**
     * 用户邮箱
     */
    @ExcelProperty("用户邮箱")
    @Schema(description = "用户邮箱")
    private String email;

    /**
     * 手机号码
     */
    @ExcelProperty("手机号码")
    @Schema(description = "手机号码")
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    @Schema(description = "用户性别(0男 1女 2未知)")
    @Dict(dictEnum = SexEnum.class)
    private Integer sex;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Schema(description = "帐号状态（0正常 1停用）")
    @Dict(dictEnum = StateEnum.class)
    private Integer state;

    /**
     * 所属部门id
     */
    @Schema(description = "所属部门id")
    private Long deptId;

    /**
     *所属部门
     */
    @Schema(description = "所属部门")
    private String deptName;

    /**
     * 帐号状态名称
     */
    @ExcelProperty("帐号状态名称")
    @Schema(description = "帐号状态名称")
    private String stateName;
    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(description = "创建人")
    private Long createUser;

    /**
     * 创建时间
     */
    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
