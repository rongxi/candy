package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * 系统字典查询条件。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "字典项-查询参数",description = "字典项-查询参数")
public class SysDictItemQueryParam extends QueryBaseParam implements Serializable {

    /**
     * 字典ID
     */
    @Schema(description = "字典ID")
    private Long dictId;

    /**
     * 字典标识
     */
    @Schema(description = "字典标识")
    private String dictCode;

    /**
     * 字典项建
     */
    @Schema(description = "字典项建")
    private String dictItemName;
}
