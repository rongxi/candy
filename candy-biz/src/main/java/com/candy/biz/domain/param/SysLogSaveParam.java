package com.candy.biz.domain.param;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 操作日志-保存参数
 *
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
@Data
@Schema(name = "操作日志-保存参数",description = "操作日志-保存参数")
public class SysLogSaveParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;

    /**
     * 模块
     */
    @Schema(description = "模块",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "模块不能为空")
    @Size(min = 1,max = 50,message = "模块长度在1和50之间")
    private String moduleName;

    /**
     * 操作类型(登录,退出,新增,修改,保存,删除,其他)
     */
    @Schema(description = "操作类型(登录,退出,新增,修改,保存,删除,其他)",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "操作类型不能为空")
    @Size(min = 1,max = 20,message = "操作类型长度在1和20之间")
    private String type;

    /**
     * 描述
     */
    @Schema(description = "描述",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "描述不能为空")
    @Size(min = 1,max = 30,message = "描述长度在1和30之间")
    private String describe;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private Long userId;

    /**
     * 用户名称
     */
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 请求ip
     */
    @Schema(description = "请求ip",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "请求ip不能为空")
    @Size(min = 1,max = 64,message = "请求ip长度在1和64之间")
    private String requestIp;

    /**
     * 请求方式
     */
    @Schema(description = "请求方式",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "请求方式不能为空")
    @Size(min = 1,max = 10,message = "请求方式长度在1和10之间")
    private String requestMethod;

    /**
     * 请求参数
     */
    @Schema(description = "请求参数",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "请求参数不能为空")
    @Size(min = 1,max = 2000,message = "请求参数长度在1和2000之间")
    private String requestParam;

    /**
     * 请求url
     */
    @Schema(description = "请求url",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "请求url不能为空")
    @Size(min = 1,max = 100,message = "请求url长度在1和100之间")
    private String requestUrl;

    /**
     * 浏览器
     */
    @Schema(description = "浏览器",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "浏览器不能为空")
    @Size(min = 1,max = 100,message = "浏览器长度在1和100之间")
    private String clientBrowser;

    /**
     * 操作系统
     */
    @Schema(description = "操作系统",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "操作系统不能为空")
    @Size(min = 1,max = 100,message = "操作系统长度在1和50之间")
    private String clientOs;

    /**
     * 请求时间
     */
    @Schema(description = "请求时间",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "请求时间不能为空")
    private LocalDateTime requestTime;

    /**
     * 耗时(毫秒)
     */
    @Schema(description = "耗时(毫秒)")
    private Long costTime;

    /**
     * 错误信息
     */
    @Schema(description = "错误信息")
    @Size(min = 1,max = 255,message = "错误信息长度在1和255之间")
    private String errorMsg;

    /**
     * 返回内容
     */
    @Schema(description = "返回内容")
    @Size(min = 1,max = 2000,message = "返回内容长度在1和2000之间")
    private String responseContent;





}
