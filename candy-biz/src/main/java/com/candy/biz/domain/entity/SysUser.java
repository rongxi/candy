package com.candy.biz.domain.entity;


import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统用户表 实体类。
 *
 * @author rong xi
 * @date 2023/09/17 21:32
 * @version  1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "sys_user", schema = "candy")
public class SysUser implements Serializable {

    /**
     * 用户ID
     */
    @Id
    private Long id;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 登录账号
     */
    private String loginAccount;

    /**
     * 密码
     */
    private String loginPassword;

    /**
     * 盐加密
     */
    private String salt;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    private Integer sex;

    /**
     * 用户类型（0超管用户 1普通用户）
     */
    private Integer type;

    /**
     * 帐号状态（0正常 1停用）
     */
    private Integer state;

    /**
     * 删除标志（0代表存在 1代表删除）
     */
    private Boolean delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 所属部门
     */
    private Long deptId;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改人
     */
    private Long updateUser;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

}
