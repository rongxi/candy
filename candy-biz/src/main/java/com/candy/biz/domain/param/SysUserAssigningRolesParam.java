package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 系统用户-分配角色参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-分配角色参数",description = "系统用户-分配角色参数")
public class SysUserAssigningRolesParam implements Serializable {

    @Schema(description = "用户id")
    @NotNull(message = "用户id不能为空")
    private Long userId;

    /**
     * 角色ids
     */
    @NotEmpty(message = "角色不能为空")
    @Schema(description = "角色ids")
    private List<Long> roleIds;

}
