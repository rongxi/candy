package com.candy.biz.domain.param;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 参数配置-保存参数
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Data
@Schema(name = "参数配置-保存参数",description = "参数配置-保存参数")
public class SysConfigSaveParam implements Serializable {
    /**
     * 参数主键
     */
    @Schema(description = "参数主键")
    private Long id;

    /**
     * 参数名称
     */
    @Schema(description = "参数名称",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "参数名称不能为空")
    @Size(min = 1,max = 30,message = "参数名称长度在1和30之间")
    private String configName;

    /**
     * 参数键名
     */
    @Schema(description = "参数键名",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "参数键名不能为空")
    @Size(min = 1,max = 30,message = "参数键名长度在1和30之间")
    private String configKey;

    /**
     * 参数键值
     */
    @Schema(description = "参数键值",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotEmpty(message = "参数键值不能为空")
    @Size(min = 1,max = 100,message = "参数键值长度在1和100之间")
    private String configValue;

    /**
     * 类型(0系统参数 1普通参数)
     */
    @Schema(description = "类型(0系统参数 1普通参数)",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "类型不能为空")
    private Integer type;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;






}
