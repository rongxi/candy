package com.candy.biz.domain.vo;

import cn.dev33.satoken.stp.SaTokenInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

/**
 * 登录结果-视图类。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "登录结果-视图类",description = "登录结果-视图类")
@Builder
@AllArgsConstructor
public class SysUserLoginVO implements Serializable {

    /**
     * 用户信息
     */
    @Schema(description = "用户信息")
    private SysUserVO user;

    /**
     * token信息
     */
    @Schema(description = "token信息")
    private SaTokenInfo token;

}
