package com.candy.biz.domain.entity;


import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;


/**
 * 菜单权限-实体类
 * 
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "sys_menu", schema = "candy")
public class SysMenu implements Serializable {

    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 名称
     */
    private String menuName;

    /**
     * 图标
     */
    private String icon;

    /**
     * 父级id
     */
    private Long parentId;

    /**
     * 类型(1菜单2权限)
     */
    private Integer type;

    /**
     * 路由地址
     */
    private String routePath;

    /**
     * 组件路径
     */
    private String componentPath;

    /**
     * 权限标识
     */
    private String permissionCode;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 删除标志(0正常 1已删除)
     */
    private Boolean delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
