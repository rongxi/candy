package com.candy.biz.domain.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统用户-数据传输类。
 *
 * @author rong xi
 * @date 2023/09/17 21:32
 * @version  1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserDTO implements Serializable {

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 登录账号
     */
    private String loginAccount;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    private Integer sex;

    /**
     * 用户类型（0超管用户 1普通用户）
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

}
