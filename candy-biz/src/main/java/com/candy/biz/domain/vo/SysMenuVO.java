package com.candy.biz.domain.vo;

import com.candy.biz.enums.MenuTypeEnum;
import com.candy.common.annotations.Dict;
import lombok.Data;
import java.io.Serializable;
import com.alibaba.excel.annotation.ExcelProperty;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 菜单权限-视图对象
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Data
@Schema(name = "菜单权限-视图对象",description = "菜单权限-视图对象")
public class SysMenuVO implements Serializable {

    /**
     * id
     */
    @ExcelProperty("id")
    @Schema(description = "id")
    private Long id;

    /**
     * 名称
     */
    @ExcelProperty("名称")
    @Schema(description = "名称")
    private String menuName;

    /**
     * 图标
     */
    @ExcelProperty("图标")
    @Schema(description = "图标")
    private String icon;

    /**
     * 父级id
     */
    @ExcelProperty("父级id")
    @Schema(description = "父级id")
    private Long parentId;

    /**
     * 类型(1菜单2权限)
     */
    @ExcelProperty("类型")
    @Schema(description = "类型(1菜单2权限)")
    @Dict(dictEnum = MenuTypeEnum.class)
    private Integer type;

    /**
     * 路由地址
     */
    @ExcelProperty("路由地址")
    @Schema(description = "路由地址")
    private String routePath;

    /**
     * 组件路径
     */
    @ExcelProperty("组件路径")
    @Schema(description = "组件路径")
    private String componentPath;

    /**
     * 权限标识
     */
    @ExcelProperty("权限标识")
    @Schema(description = "权限标识")
    private String permissionCode;

    /**
     * 顺序号
     */
    @ExcelProperty("顺序号")
    @Schema(description = "顺序号")
    private String orderNum;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Schema(description = "备注")
    private String remark;

}
