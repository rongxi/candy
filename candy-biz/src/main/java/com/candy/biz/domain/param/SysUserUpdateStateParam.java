package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;


/**
 * 系统用户-更新状态参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-更新状态参数",description = "系统用户-更新状态参数")
public class SysUserUpdateStateParam implements Serializable {

    /**
     * 用户ID
     */
    @NotNull(message = "用户id不能为空")
    @Schema(description = "用户ID")
    private Long id;


    /**
     * 帐号状态（0正常 1停用）
     */
    @NotNull(message = "用户状态不能为空")
    @Schema(description = "帐号状态（0正常 1停用）")
    private Integer state;




}
