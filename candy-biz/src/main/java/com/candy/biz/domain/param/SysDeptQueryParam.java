package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 部门-查询参数
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "部门-查询参数",description = "部门-查询参数")
public class SysDeptQueryParam extends QueryBaseParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;
    /**
     * 部门名称
     */
    @Schema(description = "部门名称")
    private String deptName;
    /**
     * 上级部门
     */
    @Schema(description = "上级部门")
    private Long parentId;
    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;
}
