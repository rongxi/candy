package com.candy.biz.domain.vo;

import com.candy.biz.enums.StateEnum;
import com.candy.common.annotations.Dict;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.io.Serializable;
/**
 * 字典项视图对象。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "字典项视图对象",description = "字典项视图对象")
public class SysDictItemVO implements Serializable {

    /**
     * 字典项id
     */
    @Schema(description = "字典项id")
    private Long id;

    /**
     * 字典id
     */
    @Schema(description = "字典id")
    private Long dictId;

    /**
     * 字典标识
     */
    @Schema(description = "字典标识")
    private String dictCode;

    /**
     * 字典项名称
     */
    @Schema(description = "字典项名称")
    private String itemName;

    /**
     * 字典项值
     */
    @Schema(description = "字典项值")
    private String itemValue;

    /**
     * 状态（0正常 1停用）
     */
    @Schema(description = "状态（0正常 1停用）")
    @Dict(dictEnum = StateEnum.class)
    private Integer state;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;


}
