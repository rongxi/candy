package com.candy.biz.domain.param;

import com.candy.common.domain.QueryBaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 操作日志-查询参数
 *
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "操作日志-查询参数",description = "操作日志-查询参数")
public class SysLogQueryParam extends QueryBaseParam implements Serializable {
    /**
     * id
     */
    @Schema(description = "id")
    private Long id;
    /**
     * 模块
     */
    @Schema(description = "模块")
    private String moduleName;
    /**
     * 操作类型(登录,退出,新增,修改,保存,删除,其他)
     */
    @Schema(description = "操作类型(登录,退出,新增,修改,保存,删除,其他)")
    private String type;
    /**
     * 描述
     */
    @Schema(description = "描述")
    private String describe;
    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private Long userId;
    /**
     * 用户名称
     */
    @Schema(description = "用户名称")
    private String userName;
    /**
     * 用户账号
     */
    @Schema(description = "用户账号")
    private String userAccount;
    /**
     * 请求ip
     */
    @Schema(description = "请求ip")
    private String requestIp;
    /**
     * 请求token
     */
    @Schema(description = "请求token")
    private String requestToken;
    /**
     * 请求时间
     */
    @Schema(description = "请求时间")
    private LocalDateTime requestTime;
    /**
     * 耗时(毫秒)
     */
    @Schema(description = "耗时(毫秒)")
    private Long costTime;
    /**
     * 错误信息
     */
    @Schema(description = "错误信息")
    private String errorMsg;
    /**
     * 返回内容
     */
    @Schema(description = "返回内容")
    private String responseContent;
}
