package com.candy.biz.domain.param;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;
import java.io.Serializable;


/**
 * 系统用户-保存参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统用户-保存参数",description = "系统用户-保存参数")
public class SysUserSaveParam implements Serializable {

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Long id;

    /**
     * 登录账号
     */
    @Size(min = 2,max = 20,message = "登录账号字符长度需要在5和20之间")
    @Schema(description = "登录账号")
    private String loginAccount;

    /**
     * 用户名称
     */
    @Size(min = 2,max = 30,message = "用户名称字符长度需要在5和30之间")
    @NotEmpty(message = "用户名称不能为空")
    @Schema(description = "用户名称")
    private String userName;

    /**
     * 用户邮箱
     */
    @Email
    @Schema(description = "用户邮箱")
    private String email;

    /**
     * 手机号码
     */
    @Size(min = 11,max = 11,message = "手机号码参数错误")
    @Schema(description = "手机号码")
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    @Min(value = 0,message = "用户性别参数错误")
    @Max(value = 2,message = "用户性别参数错误")
    @Schema(description = "用户性别（0男 1女 2未知）")
    private Integer sex;

    /**
     * 头像路径
     */
    @Schema(description = "头像路径")
    private String avatar;

    /**
     * 登录密码
     */
    @Size(min = 6,max = 30,message = "登录密码字符长度需要在6和30之间")
    @Schema(description = "登录密码")
    private String loginPassword;

    /**
     * 备注
     */
    @Size(max = 100,message = "备注字符长度不能超过100")
    @Schema(description = "备注")
    private String remark;

    @NotNull(message = "所属部门不能为空")
    @Schema(description = "所属部门")
    private Long deptId;
}
