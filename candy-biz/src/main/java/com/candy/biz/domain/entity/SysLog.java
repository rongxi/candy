package com.candy.biz.domain.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.LocalDateTime;


/**
 * 操作日志-实体类
 * 
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SysLog implements Serializable {

    /**
     * id
     */
    @Id
    @MongoId
    private Long id;

    /**
     * 模块
     */
    private String moduleName;

    /**
     * 操作类型(登录,退出,新增,修改,保存,删除,其他)
     */
    private String type;

    /**
     * 描述
     */
    private String describe;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 请求ip
     */
    private String requestIp;

    /**
     * ip归属地
     */
    private String requestRegion;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 请求url
     */
    private String requestUrl;

    /**
     * 请求token
     */
    private String requestToken;

    /**
     * 操作系统
     */
    private String clientOs;

    /**
     * 浏览器
     */
    private String clientBrowser;

    /**
     * 请求时间
     */
    private LocalDateTime requestTime;

    /**
     * 耗时(毫秒)
     */
    private Long costTime;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 返回内容
     */
    private String responseContent;

    /**
     * 创建者
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
