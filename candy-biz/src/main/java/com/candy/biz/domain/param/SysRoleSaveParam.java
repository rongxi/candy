package com.candy.biz.domain.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.io.Serializable;


/**
 * 系统角色-保存参数。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Data
@Schema(name = "系统角色-保存参数",description = "系统角色-保存参数")
public class SysRoleSaveParam implements Serializable {

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Long id;

    /**
     * 角色标识
     */
    @NotEmpty(message = "角色标识不能为空")
    @Size(min = 1,max = 30,message = "角色标识字符长度在2到30")
    @Schema(description = "角色标识")
    private String roleCode;

    /**
     * 角色名称
     */
    @NotEmpty(message = "角色名称不能为空")
    @Size(min = 1,max = 30,message = "角色名称字符长度在2到30")
    @Schema(description = "角色名称")
    private String roleName;

    /**
     * 备注
     */
    @Size(max = 100,message = "备注字符长度不能超过100")
    @Schema(description = "备注")
    private String remark;

}
