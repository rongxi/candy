package com.candy.biz.manager;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.candy.biz.domain.entity.SysLog;
import com.candy.biz.domain.param.SysLogSaveParam;
import com.candy.biz.domain.vo.SysLogVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.paginate.Page;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.List;



/**
 * 操作日志-通用处理层。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
@Component
@AllArgsConstructor
@Slf4j
public class SysLogManager {

    private static SysLogManager manager;

    public static PageVO<SysLogVO> pageToPageVO(org.springframework.data.domain.Page<SysLog> page) {
        //转换处理
        return PageVO.of(posToVos(page.getContent()),page.getTotalElements());
    }

    @PostConstruct
    private void init(){
        manager = this;
    }


    /**
     * 操作日志po对象转操作日志vo对象
     *
     * @param po po对象
     * @return vo对象
     */
    public static SysLogVO poToVo(SysLog po){
        return CommonManager.convert(po, SysLogVO.class);
    }

    /**
     * 操作日志分页对象转换分页视图数据
     *
     * @param page 分页结果
     * @return 分页数据
     */
    public static PageVO<SysLogVO> pageToPageVO(Page<SysLog> page) {
        //转换处理
        return PageVO.of(posToVos(page.getRecords()),page.getTotalRow());
    }

    /**
     * 操作日志po集合转操作日志vo集合
     *
     * @param pos po集合对象
     * @return vos vo集合对象
     */
    public static List<SysLogVO> posToVos(List<SysLog> pos){
        return CommonManager.convert(pos, SysLogVO.class);
    }


    /**
     * 新增操作日志准备
     *
     * @param param 新增参数
     * @return po对象
     */
    public static SysLog insertPrepare(SysLogSaveParam param){
        SysLog sysLog = BeanUtil.copyProperties(param, SysLog.class);
        sysLog.setId(IdUtil.getSnowflakeNextId());
        sysLog.setCreateUser(StpUtil.getLoginIdAsLong());
        sysLog.setCreateTime(LocalDateTime.now());
        return sysLog;
    }

    /**
     * 修改操作日志准备
     *
     * @param param 修改参数
     * @return po对象
     */
    public static SysLog updatePrepare(SysLogSaveParam param){
        SysLog sysLog = BeanUtil.copyProperties(param, SysLog.class);
        sysLog.setUpdateUser(StpUtil.getLoginIdAsLong());
        sysLog.setUpdateTime(LocalDateTime.now());
        return sysLog;
    }

}
