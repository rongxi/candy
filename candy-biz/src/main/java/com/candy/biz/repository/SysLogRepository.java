package com.candy.biz.repository;

import com.candy.biz.domain.entity.SysLog;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SysLogRepository extends MongoRepository<SysLog, Long> {

}