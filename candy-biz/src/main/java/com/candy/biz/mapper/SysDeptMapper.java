package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门-映射层。
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
