package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户表 映射层。
 *
 * @author rong xi
 * @since 1.0.0
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
