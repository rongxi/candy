package com.candy.biz.mapper;

import com.candy.biz.domain.entity.SysRoleMenu;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色菜单关联-映射层。
 *
 * @author rong xi
 * @date 2023/10/08 09:23
 * @version 1.0.0
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
