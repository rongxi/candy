package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysDictItem;

/**
 * 字典项 映射层。
 *
 * @author rong xi
 * @since 1.0.0
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
