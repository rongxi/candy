package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 参数配置-映射层。
 *
 * @author rong xi
 * @date 2023/09/26 16:52
 * @version 1.0.0
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
