package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysDict;

/**
 * 字典 映射层。
 *
 * @author rong xi
 * @since 1.0.0
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
