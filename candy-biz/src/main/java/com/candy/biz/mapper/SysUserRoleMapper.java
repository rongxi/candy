package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色关联-映射层。
 *
 * @author rong xi
 * @date 2023/10/08 09:23
 * @version 1.0.0
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
