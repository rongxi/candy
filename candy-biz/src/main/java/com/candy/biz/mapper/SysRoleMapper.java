package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysRole;

/**
 * 系统角色-映射层。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
