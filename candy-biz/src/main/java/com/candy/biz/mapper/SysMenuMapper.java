package com.candy.biz.mapper;

import com.mybatisflex.core.BaseMapper;
import com.candy.biz.domain.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单权限-映射层。
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
