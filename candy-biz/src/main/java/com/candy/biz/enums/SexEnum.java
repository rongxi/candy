package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Arrays;

/**
 * 性别枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum SexEnum{
    MALE(0,"男"),
    FEMALE(1,"女"),
    UNKNOWN(2,"未知");

    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(SexEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(SexEnum::getRemark)
                .orElse(null);
    }
}
