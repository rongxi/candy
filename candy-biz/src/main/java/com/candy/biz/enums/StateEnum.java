package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 状态枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum StateEnum {
    NORMAL(0,"正常"),
    DISABLE(1,"禁用"),
    ;

    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(StateEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(StateEnum::getRemark)
                .orElse(null);
    }
}
