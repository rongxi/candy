package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统参数类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum ConfigKeyEnum {


    ENABLE_CAPTCHA("enableCaptcha","是否开启验证码"),
    LOGIN_ERROR_DISABLE_TIME("loginFailDisableNum","登录失败禁用次数")
    ;
    private final String key;
    private final String remark;

}
