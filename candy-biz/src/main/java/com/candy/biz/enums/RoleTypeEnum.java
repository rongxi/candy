package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 角色类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum RoleTypeEnum {
    SYSTEM(0,"系统角色"),

    ORDINARY(1,"普通角色"),
    ;
    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(RoleTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(RoleTypeEnum::getRemark)
                .orElse(null);
    }
}
