package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 参数类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum ConfigTypeEnum {

    SYSTEM(0,"系统参数"),
    ORDINARY(1,"普通参数"),
    ;
    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(ConfigTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(ConfigTypeEnum::getRemark)
                .orElse(null);
    }
}
