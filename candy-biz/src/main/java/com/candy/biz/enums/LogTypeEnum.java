package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 操作日志类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum LogTypeEnum {
    LOGIN("LOGIN","登录"),
    LOGIN_OUT("LOGIN_OUT","退出"),
    ADD("ADD","新增"),
    REMOVE("REMOVE","删除"),
    EDIT("EDIT","修改"),
    QUERY("QUERY","查询"),
    OTHER("OTHER","其他"),
    SAVE("SAVE","保存"),
    ;
    private final String value;
    private final String remark;

    public static String getRemarkByValue(String value){
        return Arrays.stream(LogTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(LogTypeEnum::getRemark)
                .orElse(null);
    }
}
