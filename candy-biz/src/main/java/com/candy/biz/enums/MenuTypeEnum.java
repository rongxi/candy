package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 菜单类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum MenuTypeEnum {
    MENU(1,"菜单"),
    PERMISSION(2,"权限"),
    ;
    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(MenuTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(MenuTypeEnum::getRemark)
                .orElse(null);
    }
}
