package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 用户类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum UserTypeEnum {
    SYSTEM(0,"超管用户"),

    ORDINARY(1,"普通用户"),
    ;
    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(UserTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(UserTypeEnum::getRemark)
                .orElse(null);
    }
}
