package com.candy.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 字典类型枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/15 21:40
 */
@AllArgsConstructor
@Getter
public enum DictTypeEnum {
    SYSTEM(0,"系统字典"),
    ORDINARY(1,"普通字典"),
    ;
    private final Integer value;
    private final String remark;

    public static String getRemarkByValue(Integer value){
        return Arrays.stream(DictTypeEnum.values())
                .filter(e->e.getValue().equals(value))
                .findAny()
                .map(DictTypeEnum::getRemark)
                .orElse(null);
    }
}
