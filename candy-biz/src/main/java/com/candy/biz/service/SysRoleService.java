package com.candy.biz.service;

import com.candy.biz.domain.param.SysRoleAssigningMenusParam;
import com.candy.biz.domain.param.SysRoleQueryParam;
import com.candy.biz.domain.param.SysRoleSaveParam;
import com.candy.biz.domain.vo.SysRoleVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysRole;
import java.util.List;

/**
 * 系统角色-服务层接口。
 *
 * @author rong xi
 * @date 2023/09/18 09:18
 * @version 1.0.0
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 查询系统角色详情
     *
     * @param id 系统角色id
     * @return VO对象
     */
    SysRoleVO queryInfo(Long id);

    /**
     * 分页查询系统角色
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysRoleVO> queryPage(SysRoleQueryParam param);

    /**
     * 根据查询条件导出数据
     *
     * @param param 查询参数
     */
    void export(SysRoleQueryParam param);

    /**
     * 保存系统角色
     *
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysRoleSaveParam param);

    /**
     * 根据条件查询角色列表
     *
     * @param param 查询参数
     * @return 查询结果
     */
    List<SysRoleVO> queryList(SysRoleQueryParam param);

    /**
     * 保存角色分配菜单
     *
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean assigningMenu(SysRoleAssigningMenusParam param);

    /**
     * 查询已分配的菜单
     *
     * @param id 角色id
     * @return 菜单id列表
     */
    List<Long> queryAssignedMenu(Long id);

    /**
     * 删除角色
     *
     * @param id 角色id
     * @return 删除结果
     */
    Boolean removeRole(Long id);
}
