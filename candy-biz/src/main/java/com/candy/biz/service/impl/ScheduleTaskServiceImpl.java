package com.candy.biz.service.impl;

import com.candy.biz.service.ScheduleTaskService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * 定时任务实现类
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/12 11:08
 */
@Slf4j
@Service
@AllArgsConstructor
public class ScheduleTaskServiceImpl implements ScheduleTaskService {
    private static final String DEMO_TASK = "demo任务";

    /**
     * demo任务
     */
    @Override
    public void demoTask() {
        String name = Thread.currentThread().getName();
        try{
            log.info(name+"开始执行"+DEMO_TASK);

            log.info(DEMO_TASK+"执行完毕");
        }catch (Exception e){
            log.error(DEMO_TASK+"出现异常",e);
        }
    }
}
