package com.candy.biz.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONObject;
import com.candy.biz.domain.dto.SysUserDTO;
import com.candy.biz.domain.entity.SysLog;
import com.candy.biz.manager.SysUserManager;
import com.candy.biz.repository.SysLogRepository;
import com.candy.biz.service.SysLogService;
import com.candy.common.interfaces.OpLogSaveService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * 保存操作日志服务
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/13 11:32
 */
@Service
@AllArgsConstructor
@Slf4j
public class OpLogSaveServiceImpl implements OpLogSaveService {
    private final SysLogRepository sysLogRepository;

    /**
     * 保存操作日志
     *
     * @param jsonObject 日志信息
     * @return 任务
     */
    @Override
    public Runnable save(JSONObject jsonObject) {
        return ()->{
            SysLog sysLog = jsonObject.toBean(SysLog.class);
            sysLog.setCreateTime(LocalDateTime.now());
            sysLog.setCreateUser(sysLog.getUserId());
            Optional<SysUserDTO> userInfoOpt = SysUserManager.getUserInfoOpt(sysLog.getUserId());
            sysLog.setUserAccount(userInfoOpt.map(SysUserDTO::getLoginAccount).orElse(""));
            sysLog.setUserName(userInfoOpt.map(SysUserDTO::getUserName).orElse(""));
            sysLog.setId(IdUtil.getSnowflakeNextId());
            log.debug("待插入的日志对象:{}",sysLog);
            sysLogRepository.save(sysLog);
        };
    }
}
