package com.candy.biz.service;

/**
 * 定时任务服务
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/12 11:07
 */
public interface ScheduleTaskService {

    /**
     * 演示任务
     */
    void demoTask();
}
