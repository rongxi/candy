package com.candy.biz.service;

import com.candy.biz.domain.param.SysLogQueryParam;
import com.candy.biz.domain.param.SysLogSaveParam;
import com.candy.biz.domain.vo.OnlineUserVO;
import com.candy.biz.domain.vo.SysLogVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysLog;

/**
 * 操作日志-服务层接口。
 *
 * @author rong xi
 * @date 2023/10/11 17:54
 * @version 1.0.0
 */
public interface SysLogService {

    /**
     * 查询操作日志详情
     *
     * @param id 操作日志id
     * @return VO对象
     */
    SysLogVO queryInfo(Long id);

    /**
     * 分页查询操作日志
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysLogVO> queryPage(SysLogQueryParam param);

    /**
     * 分页查询在线用户
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<OnlineUserVO> queryOnlinePage(SysLogQueryParam param);

    /**
     * 根据查询条件导出操作日志
     *
     * @param param 查询参数
     */
    void export(SysLogQueryParam param);

    /**
     * 保存操作日志
     *
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysLogSaveParam param);

    /**
     * 强退用户
     *
     * @param token 用户token
     * @return 强退结果
     */
    Boolean tickOutUser(String token);
}

