package com.candy.biz.service;

import com.candy.biz.domain.param.SysDictItemQueryParam;
import com.candy.biz.domain.param.SysDictItemSaveParam;
import com.candy.biz.domain.vo.DictSelectItemVO;
import com.candy.biz.domain.vo.SysDictItemVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysDictItem;
import java.util.List;

/**
 * 字典项 服务层。
 *
 * @author rong xi
 * @since 1.0.0
 */
public interface SysDictItemService extends IService<SysDictItem> {

    /**
     * 分页查询字典项
     *
     * @param param 分页参数
     * @return 分页结果
     */
    List<SysDictItemVO> queryList(SysDictItemQueryParam param);

    /**
     * 查询字典下拉框选项
     *
     * @param dictCode 字典code
     * @return 选项集合
     */
    List<DictSelectItemVO> queryDictSelectItem(String dictCode);

    /**
     * 保存字典项
     *
     * @param param 字典项参数
     * @return 保存结果
     */
    Boolean saveHandle(SysDictItemSaveParam param);

    /**
     * 删除字典项
     *
     * @param id 字典项id
     * @return 删除结果
     */
    Boolean removeHandle(Long id);


}
