package com.candy.biz.service;

import com.candy.biz.domain.param.SysDeptQueryParam;
import com.candy.biz.domain.param.SysDeptSaveParam;
import com.candy.biz.domain.vo.SysDeptVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysDept;
import java.util.List;
import cn.hutool.core.lang.tree.Tree;

/**
 * 部门-服务层接口。
 *
 * @author rong xi
 * @date 2023/10/26 13:33
 * @version 1.0.0
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 查询部门详情
     *
     * @param id 部门id
     * @return VO对象
     */
    SysDeptVO queryInfo(Long id);

    /**
     * 分页查询部门
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysDeptVO> queryPage(SysDeptQueryParam param);

    /**
     * 根据条件查询部门树集合。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    List<Tree<Long>> queryTreeList(SysDeptQueryParam param);

    /**
     * 根据条件查询部门树。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    Tree<Long> queryTree(SysDeptQueryParam param);
    /**
     * 根据查询条件导出部门
     *
     * @param param 查询参数
     */
    void export(SysDeptQueryParam param);

    /**
     * 保存部门
     *
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysDeptSaveParam param);


}

