package com.candy.biz.service;


import com.candy.biz.domain.param.*;
import com.candy.biz.domain.vo.SysUserLoginVO;
import com.candy.biz.domain.vo.SysUserVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysUser;
import java.util.List;


/**
 * 系统用户表 服务层。
 *
 * @author rong xi
 * @since 1.0.0
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 分页查询用户列表
     *
     * @param param 查询参数
     * @return 用户列表
     */
    PageVO<SysUserVO> queryPage(SysUserQueryParam param);
    /**
     * 更新用户信息
     *
     * @param param 更新参数
     * @return 更新结果
     */
    Boolean saveHandle(SysUserSaveParam param);
    /**
     * 导出用户信息
     *
     * @param param 查询参数
     */
    void export(SysUserQueryParam param);
    /**
     * 查询用户详情
     *
     * @param id 用户id
     * @return 用户详情
     */
    SysUserVO queryInfo(Long id);
    /**
     * 重置登录密码
     *
     * @param param 重置参数
     * @return 重置结果
     */
    Boolean resetPassword(SysUserResetPasswordParam param);
    /**
     * 更新用户状态。
     *
     * @param param 更新参数
     * @return 更新结果
     */
    Boolean updateState(SysUserUpdateStateParam param);

    /**
     * 登录。
     *
     * @param param 登录参数
     * @return 登录结果
     */
    SysUserLoginVO login(SysUserLoginParam param);

    /**
     * 退出登录
     *
     * @return 退出结果
     */
    Boolean loginOut();

    /**
     * 分配角色
     *
     * @param param 角色参数
     * @return 分配结果
     */
    Boolean assigningRoles(SysUserAssigningRolesParam param);

    /**
     * 查询用户已分配的角色ids
     *
     * @param id 用户id
     * @return 角色ids
     */
    List<Long> queryAssignedRole(Long id);

    /**
     * 获取验证码图片
     *
     * @param clientId 客户端id
     */
    void getCaptchaImg(String clientId);

    /**
     * 删除用户
     *
     * @param id 用户id
     * @return 删除结果
     */
    Boolean removeUser(Long id);
}
