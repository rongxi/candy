package com.candy.biz.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import com.candy.biz.service.SysMenuService;
import com.candy.biz.service.SysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * 自定义权限加载接口实现类。
 * 保证此类被 SpringBoot 扫描，完成 Sa-Token 的自定义权限验证扩展
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Component
@AllArgsConstructor
@Slf4j
public class StpInterfaceImpl implements StpInterface {

    private final SysMenuService sysMenuService;
    private final SysUserService sysUserService;

    /**
     * 获取权限集合
     *
     * @param userId 用户id
     * @param loginType 登录方式
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object userId, String loginType) {
        return sysMenuService.queryPermissions(Long.valueOf(userId.toString()));
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     *
     * @param userId 用户id
     * @param loginType 登录方式
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getRoleList(Object userId, String loginType) {
        return sysUserService.queryAssignedRole(Long.valueOf(userId.toString()))
                .stream().map(String::valueOf).toList();
    }

}
