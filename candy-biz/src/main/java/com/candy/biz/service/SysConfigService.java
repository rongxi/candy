package com.candy.biz.service;

import com.candy.biz.domain.entity.SysConfig;
import com.candy.biz.domain.param.SysConfigQueryParam;
import com.candy.biz.domain.param.SysConfigSaveParam;
import com.candy.biz.domain.vo.SysConfigVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;

/**
 * 参数配置-服务层接口。
 *
 * @author rong xi
 * @date 2023/09/26 16:52
 * @version 1.0.0
 */
public interface SysConfigService extends IService<SysConfig> {

    /**
     * 查询参数配置详情
     * @param id 参数配置id
     * @return VO对象
     */
    SysConfigVO queryInfo(Long id);

    /**
     * 查询参数配置值
     *
     * @param key 参数配置key
     * @return 配置值
     */
    String getValue(String key);

    /**
     * 分页查询参数配置
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysConfigVO> queryPage(SysConfigQueryParam param);

    /**
     * 根据查询条件导出参数配置
     * @param param 查询参数
     */
    void export(SysConfigQueryParam param);

    /**
     * 保存参数配置
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysConfigSaveParam param);

    /**
     * 删除配置
     *
     * @param id 配置id
     * @return 删除结果
     */
    Boolean removeConfig(Long id);
}

