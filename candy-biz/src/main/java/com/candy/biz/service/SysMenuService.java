package com.candy.biz.service;

import com.candy.biz.domain.param.SysMenuQueryParam;
import com.candy.biz.domain.param.SysMenuSaveParam;
import com.candy.biz.domain.vo.SysMenuVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysMenu;
import java.util.List;
import cn.hutool.core.lang.tree.Tree;

/**
 * 菜单权限-服务层接口。
 *
 * @author rong xi
 * @date 2023/10/07 15:28
 * @version 1.0.0
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 查询菜单权限详情
     *
     * @param id 菜单权限id
     * @return VO对象
     */
    SysMenuVO queryInfo(Long id);

    /**
     * 分页查询菜单权限
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysMenuVO> queryPage(SysMenuQueryParam param);

    /**
     * 根据条件查询菜单权限树集合。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    List<Tree<Long>> queryTreeList(SysMenuQueryParam param);

    /**
     * 根据条件查询菜单权限树。
     *
     * @param param 查询参数
     * @return 查询结果
     */
    Tree<Long> queryTree(SysMenuQueryParam param);
    /**
     * 根据查询条件导出菜单权限
     *
     * @param param 查询参数
     */
    void export(SysMenuQueryParam param);

    /**
     * 保存菜单权限
     *
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysMenuSaveParam param);

    /**
     * 查询用户菜单列表。
     *
     * @param userId 用户id
     * @return 菜单集合
     */
    List<Tree<String>> queryMenus(Long userId);
    /**
     * 查询用户权限集合。
     * @param userId 用户id
     * @return 权限集合
     */
    List<String> queryPermissions(Long userId);
}

