package com.candy.biz.service;

import com.candy.biz.domain.param.SysDictQueryParam;
import com.candy.biz.domain.param.SysDictSaveParam;
import com.candy.biz.domain.vo.SysDictVO;
import com.candy.common.domain.PageVO;
import com.mybatisflex.core.service.IService;
import com.candy.biz.domain.entity.SysDict;
import java.util.List;

/**
 * 字典 服务层。
 *
 * @author rong xi
 * @since 1.0.0
 */
public interface SysDictService extends IService<SysDict> {

    /**
     * 查询字典详情
     *
     * @param id 字典id
     * @return VO对象
     */
    SysDictVO queryInfo(Long id);

    /**
     * 查询字典列表
     * @param param 查询条件
     * @return 字典列表
     */
    List<SysDictVO> queryList(SysDictQueryParam param);

    /**
     * 分页查询字典
     *
     * @param param 查询参数
     * @return 分页结果
     */
    PageVO<SysDictVO> queryPage(SysDictQueryParam param);

    /**
     * 保存字典
     * @param param 保存参数
     * @return 保存结果
     */
    Boolean saveHandle(SysDictSaveParam param);

    /**
     * 删除字典
     * @param id 字典id
     * @return 删除结果
     */
    Boolean removeHandle(Long id);



}
