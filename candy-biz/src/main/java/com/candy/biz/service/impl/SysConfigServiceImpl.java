package com.candy.biz.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.candy.biz.domain.entity.SysConfig;
import com.candy.biz.domain.param.SysConfigQueryParam;
import com.candy.biz.domain.param.SysConfigSaveParam;
import com.candy.biz.domain.vo.SysConfigVO;
import com.candy.biz.enums.ConfigTypeEnum;
import com.candy.biz.manager.SysConfigManager;
import com.candy.biz.mapper.SysConfigMapper;
import com.candy.biz.service.SysConfigService;
import com.candy.common.domain.PageVO;
import com.candy.common.enums.AdminErrorEnum;
import com.candy.common.enums.RedisKeyEnum;
import com.candy.common.manager.RedisManager;
import com.candy.common.utils.Assert;
import com.candy.common.utils.CommonUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.If;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.candy.biz.domain.entity.table.SysConfigTableDef.SYS_CONFIG;

/**
 * 参数配置-服务层实现。
 *
 * @author rong xi
 * @date 2023/10/08 20:52
 * @version 1.0.0
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    /**
     * 查询参数配置详情
     *
     * @param id 参数配置id
     * @return 详情VO
     */
    @Override
    public SysConfigVO queryInfo(Long id) {
        return this.getByIdOpt(id).map(SysConfigManager::poToVo).orElse(null);
    }

    /**
     * 查询参数配置值
     *
     * @param key 参数配置key
     * @return 配置值
     */
    @Override
    public String getValue(String key) {
        return SysConfigManager.getConfigValueOpt(key).orElse(null);
    }

    /**
     * 分页查询参数配置
     *
     * @param param 查询参数
     * @return 分页结果
     */
    @Override
    public PageVO<SysConfigVO> queryPage(SysConfigQueryParam param) {
        Page<SysConfig> page = queryChain()
                .select(SYS_CONFIG.ALL_COLUMNS)
                .from(SYS_CONFIG)
                .where(SYS_CONFIG.ID.eq(param.getId(), If::notNull))
                .and(SYS_CONFIG.ID.eq(param.getId(), If::notNull))
                .and(SYS_CONFIG.CONFIG_NAME.like(param.getConfigName(), If::hasText))
                .and(SYS_CONFIG.CONFIG_KEY.eq(param.getConfigKey(), If::hasText))
                .and(SYS_CONFIG.TYPE.eq(param.getType(), If::notNull))
                .orderBy(param.getOrderBy(SYS_CONFIG))
                .page(param.getPage());
        return SysConfigManager.pageToPageVO(page);
    }


    /**
     * 根据查询条件导出参数配置。
     *
     * @param param 查询条件
     */
    @Override
    @SneakyThrows
    public void export(SysConfigQueryParam param) {
        //修改分页条件
        param.preparationExport();
        //查询数据
        List<SysConfigVO> rows = queryPage(param).getList();
        //导出文件
        CommonUtil.exportExcel("参数配置",rows,SysConfigVO.class);
    }


    /**
     * 保存参数配置
     *
     * @param param 保存参数
     * @return 保存结果
     */
    @Override
    public Boolean saveHandle(SysConfigSaveParam param) {
        return CommonUtil.<SysConfigSaveParam,Boolean>predicateFunction(param,
                (p)->ObjectUtil.isNull(p.getId()),
                (p)->this.save(SysConfigManager.insertPrepare(p)),
                (p)->{
                    RedisManager.delete(RedisKeyEnum.CONFIG.getKey());
                    return this.updateById(SysConfigManager.updatePrepare(p));
                });
    }

    /**
     * 删除配置
     *
     * @param id 配置id
     * @return 删除结果
     */
    @Override
    public Boolean removeConfig(Long id) {
        SysConfig sysConfig = this.getById(id);
        Assert.notNull(sysConfig, AdminErrorEnum.CONFIG_NOT_EXISTS);
        Assert.notEquals(ConfigTypeEnum.SYSTEM.getValue(),sysConfig.getType(),AdminErrorEnum.SYSTEM_CONFIG_CANT_NOT_DELETE);
        //删除参数
        return removeById(id);
    }
}
