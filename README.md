
<center>
    <h1>
        candy
    </h1>
    <img src="https://img.shields.io/badge/link-996.icu-red.svg" alt="996.icu">
    <img src="https://img.shields.io/badge/Spring%20Boot-3.2.0-blue.svg" alt="SpringBoot">
    <img src="https://img.shields.io/badge/SaToken-1.37.0-blue.svg" alt="SaToken">
    <img src="https://img.shields.io/badge/MyBatisFlex-1.7.5-blue.svg" alt="MyBatisFlex">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="license"/>
</center>

### 项目简介
candy是一个SpringBoot3+vue3单体前后端分离的权限管理系统。本人在之前的工作中接触了好些优秀的管理系统，比如[若依](http://www.ruoyi.vip/)、[Guns](https://www.javaguns.com/)及[pigx](https://www.pig4cloud.com/)等，受他们的启发
遂萌生了要搭建一个符合本人编程习惯的系统，以此来满足个人学习最新技术的需要，此为后端代码，前端代码在[candy-ui](https://gitee.com/rongxi/candy-ui)，项目刚开源，
有很多不足之处亟待优化，仅供大家学习使用。

### 软件架构

| 依赖           | 版本      | 网址                                         |
|--------------|---------|--------------------------------------------|
| jdk          | 21      | https://www.oracle.com/java                |
| mysql        | 8.0+    | https://dev.mysql.com/downloads/mysql/     |
| redis        | 7.0+    | https://redis.io/                          |
| mongodb      | 7.0+    | https://www.mongodb.com/                   |
| SpringBoot   | 3.2.0   | https://spring.io/projects/spring-boot     |
| Sa-Token     | 1.37.0  | https://sa-token.cc/                       |
| MyBatis-Flex | 1.7.5   | https://mybatis-flex.com/                  |
| hutool       | 5.8.23  | https://www.hutool.cn/                     |
| fastjson2    | 2.0.42  | https://github.com/alibaba/fastjson2       |
| lombok       | 1.18.30 | https://projectlombok.org/                 |
| easyExcel    | 3.3.2   | https://easyexcel.opensource.alibaba.com/  |
| knife4j      | 4.3.0   | https://doc.xiaominfo.com/docs/quick-start |


### 安装教程

1.  [安装](https://blog.csdn.net/m0_69750058/article/details/131628553)/升级jdk21([新特性](https://www.didispace.com/java-features/#java-21))
2.  clone代码/下载zip文件并导入项目到idea中
3.  创建账号及candy数据库 
4.  执行初始化sql
5.  启动服务

### 模块说明

1.  candy-admin - 系统管理服务-controller、task
2.  candy-biz - 业务服务-包括service、mapper、manager等
3.  candy-codegen - 代码生成器-在若依代码生成器技术上修改而来，配置和依赖已分离，如需要可单独才分出来
4.  candy-common - 通用配置
5.  candy-monitor- 服务监控模块

### 系统界面
![image.png](https://betpay.oss-cn-hongkong.aliyuncs.com/picture/login.png)
![image.png](https://betpay.oss-cn-hongkong.aliyuncs.com/picture/user.png)
![image.png](https://betpay.oss-cn-hongkong.aliyuncs.com/picture/code-gen.png)

### 使用说明
请移步到[WIKI](https://gitee.com/rongxi/candy/wikis/pages)

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 路线图

1. 增加防重复提交 √
2. 增加租户管理
3. 增加多数据源
4. 增加国际化
5. 简化代码生成器
6. 支持docker  √
7. 引入分布式锁  √
8. 增加系统公告
9. 增加XSS过滤  √
10. 增加服务监控 √
11. ip地址归属地 √
12. 在线用户 √


### 备注
1. [阿里巴巴java开发手册](https://developer.aliyun.com/special/tech-java)
2. [程序猿DD](https://www.didispace.com/)
3. [云效](https://devops.aliyun.com/workbench)
4. [豆包](https://www.doubao.com/chat/)
5. [JavaGuide](https://javaguide.cn/)
6. [友谊万岁](https://bafybeigbys4tqtwc6xfuatw2vx6u7uwchj7pp7kjytt6uort2uh74dnq2u.ipfs.nftstorage.link/)

### 联系作者
420154195@qq.com

### 更新日志

#### v0.9.00 2023-10-27
1. 增加部门管理
2. 修改系统字典、角色、用户无法删除功能
3. 增加登录错误锁定用户
4. 修复若干bug
5. 规范化代码
6. 增加监控模块
#### v0.9.01 2023-11-06
1. 增加配置文件加密
2. 增加XSS过滤器
3. 增加系统属性配置
4. 增加ip地址归属地
5. 增加在线用户
#### v0.9.01 2023-11-30
1.升级到springboot3.2