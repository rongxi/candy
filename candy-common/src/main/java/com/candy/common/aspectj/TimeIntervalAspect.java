package com.candy.common.aspectj;

import cn.hutool.aop.aspects.SimpleAspect;
import cn.hutool.core.date.TimeInterval;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * 计时器
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Slf4j
public class TimeIntervalAspect extends SimpleAspect {

	private final TimeInterval interval = new TimeInterval();

	@Override
	public boolean before(Object target, Method method, Object[] args) {
		interval.start();
		return true;
	}

	@Override
	public boolean after(Object target, Method method, Object[] args,Object returnVal) {
		log.info("Method [{}.{}] execute spend [{}]ms", target.getClass().getName(), method.getName(), interval.intervalMs());
		return true;
	}



}
