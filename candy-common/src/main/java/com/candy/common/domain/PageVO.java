package com.candy.common.domain;

import com.mybatisflex.core.paginate.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 通用分页传输对象
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVO<T> implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * 返回的数据
	 */
	private List<T> list;

	/**
	 * 总条数
	 */
	private Long total;

	public static <T> PageVO<T> of(Page<T> page) {
		return new PageVO<>(page.getRecords(),page.getTotalRow());
	}

	public static <T> PageVO<T> of(List<T> list, Long total) {
		return new PageVO<>(list,total);
	}


}
