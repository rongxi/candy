package com.candy.common.interfaces;

import cn.hutool.json.JSONObject;

/**
 * 创建任务接口
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/13 11:18
 */
@FunctionalInterface
public interface OpLogSaveService{

    /**
     * 保存操作日志
     *
     * @param jsonObject 日志信息
     * @return 任务
     */
    Runnable save(JSONObject jsonObject);
}
