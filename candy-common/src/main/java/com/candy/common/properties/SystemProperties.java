package com.candy.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 系统xss过滤配置属性
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/11/07 14:16
 */
@ConfigurationProperties(prefix = "candy.system")
@Component
@Data
public class SystemProperties {

    /**
     * 是否开启全局XSS过滤
     */
    Boolean xssEnabled = true;

    /**
     * XSS排除路径
     */
    String xssExcludes;

    /**
     * 是否开启跨域
     */
    Boolean corsEnabled = true;

    /**
     * 是否开启全局重复提交检查
     */
    Boolean dsEnabled = true;

    /**
     * 防重复提交排除路径
     */
    String dsExcludes;

    /**
     * 重复提交间隔(毫秒)
     */
    Long dsInterval = 1000L;


}
