package com.candy.common.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.candy.common.interceptor.DsInterceptor;
import com.candy.common.properties.SystemProperties;
import com.candy.common.utils.CommonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * web配置类
 *
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Configuration
@Slf4j
@AllArgsConstructor
public class WebConfig implements WebMvcConfigurer {
    private static final String DEFAULT_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

    private final DsInterceptor dsInterceptor;
    /**
     * 注册 Sa-Token 拦截器，打开注解式鉴权功能
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(dsInterceptor)
                .addPathPatterns("/**");

        registry.addInterceptor(new SaInterceptor(handle -> StpUtil.checkLogin()))
                .addPathPatterns("/**")
                .excludePathPatterns("/sys-user/login","/sys-user/login-out","/sys-user/captcha-img/*","/favicon.ico" ,"/error", "/doc.html" , "/webjars/**" , "/swagger-resources/**" , "/*/api-docs/**" , "/swagger-ui.html")
                .excludePathPatterns("/sys-config/get-value/*");
    }


    @Primary
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customJackson() {
        return jacksonObjectMapperBuilder -> {
            //序列化包含null的属性
            jacksonObjectMapperBuilder.serializationInclusion(JsonInclude.Include.ALWAYS);
            jacksonObjectMapperBuilder.failOnUnknownProperties(false);
            // jacksonObjectMapperBuilder.propertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
            jacksonObjectMapperBuilder.failOnEmptyBeans(false);
            //jacksonObjectMapperBuilder.featuresToEnable(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS,JsonReadFeature.ALLOW_SINGLE_QUOTES);
            jacksonObjectMapperBuilder.dateFormat(new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN));
            DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_PATTERN);
            jacksonObjectMapperBuilder.serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(localDateTimeFormatter));
            jacksonObjectMapperBuilder.deserializerByType(LocalDateTime.class, new LocalDateTimeDeserializer(localDateTimeFormatter));
            DateTimeFormatter localDateFormatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_PATTERN);
            jacksonObjectMapperBuilder.serializerByType(LocalDate.class, new LocalDateSerializer(localDateFormatter));
            jacksonObjectMapperBuilder.deserializerByType(LocalDate.class, new LocalDateDeserializer(localDateFormatter));
            jacksonObjectMapperBuilder.serializerByType(BigDecimal.class, new BigDecimalSerializer());
            jacksonObjectMapperBuilder.deserializerByType(Date.class, new DateDeserializer());
            jacksonObjectMapperBuilder.serializerByType(Long.TYPE, ToStringSerializer.instance);
            jacksonObjectMapperBuilder.serializerByType(Long.class, ToStringSerializer.instance);

        };
    }

    /**
     * BigDecimal序列化实现
     */
    public static class BigDecimalSerializer extends JsonSerializer<BigDecimal> {
        @Override
        public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) {
            CommonUtil.notNullThen(value,(v)-> {
                try {
                    gen.writeString(value.stripTrailingZeros().toPlainString());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }


    /**
     * Date反序列化实现
     */
    public static class DateDeserializer extends JsonDeserializer<Date>{

        @Override
        public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return CommonUtil.predicateFunction(jsonParser.getText(),
                    StrUtil::isNotEmpty,
                    (str) -> CommonUtil.predicateFunction(str.trim(),
                                                    s -> s.length() == 10,
                                                    (s) -> DateUtil.parse(s, DEFAULT_DATE_PATTERN),
                                                    (s) -> DateUtil.parse(s, DEFAULT_DATE_TIME_PATTERN)),
                    (s)->null);
        }
    }

    /**
     * 注册 [Sa-Token 全局过滤器]
     */
    @Bean
    @ConditionalOnProperty(prefix = "candy.system", name = "cors-enabled", havingValue = "true")
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter()
                // 前置函数：在每次认证函数之前执行
                .setBeforeAuth(obj -> {

                    SaHolder.getResponse()
                            // ---------- 设置跨域响应头 ----------
                            // 允许指定域访问跨域资源
                            .setHeader("Access-Control-Allow-Origin", "*")
                            // 允许所有请求方式
                            .setHeader("Access-Control-Allow-Methods", "*")
                            // 允许的header参数
                            .setHeader("Access-Control-Allow-Headers", "*")
                            // 有效时间
                            .setHeader("Access-Control-Max-Age", "3600");
                    // 如果是预检请求，则立即返回到前端
                    SaRouter.match(SaHttpMethod.OPTIONS).back();
                });
    }

}
