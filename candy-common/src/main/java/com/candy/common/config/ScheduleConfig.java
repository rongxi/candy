package com.candy.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


/**
 * 定时任务配置类
 *
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@EnableScheduling
@Configuration
@EnableAsync
public class ScheduleConfig {

    @Bean(name = "myThreadPoolTaskExecutor")
    public ThreadPoolTaskExecutor getMyThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        //获取核心数
        int i = Runtime.getRuntime().availableProcessors();
        taskExecutor.setCorePoolSize(i * 2);
        taskExecutor.setMaxPoolSize(i * 2);
        taskExecutor.setQueueCapacity(i * 2 * 100);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setThreadNamePrefix("my-task-");
        taskExecutor.initialize();
        return taskExecutor;
    }
}