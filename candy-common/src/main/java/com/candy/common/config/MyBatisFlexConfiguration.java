package com.candy.common.config;

import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.datasource.DataSourceDecipher;
import com.mybatisflex.core.datasource.DataSourceProperty;
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisFlex配置类
 *
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Configuration
public class MyBatisFlexConfiguration implements MyBatisFlexCustomizer {

    private static final Logger logger = LoggerFactory
        .getLogger("mybatis-flex-sql");

    public MyBatisFlexConfiguration() {
        //开启审计功能
        AuditManager.setAuditEnable(true);
        //设置 SQL 审计收集器
        AuditManager.setMessageCollector(auditMessage ->
            logger.info("{},{}ms", auditMessage.getFullSql()
                , auditMessage.getElapsedTime())
        );
    }

    @Override
    public void customize(FlexGlobalConfig flexGlobalConfig) {
        flexGlobalConfig.setLogicDeleteColumn("del_flag");
        //设置数据库正常时的值
        flexGlobalConfig.setNormalValueOfLogicDelete(0);
        //设置数据已被删除时的值
        flexGlobalConfig.setDeletedValueOfLogicDelete(1);
        //设置乐观锁字段
        flexGlobalConfig.setVersionColumn("version");
    }

}