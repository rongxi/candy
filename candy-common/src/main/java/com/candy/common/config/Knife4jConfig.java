package com.candy.common.config;

import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Knife4j配置类
 *
 * @author rong xi
 * @date 2023/09/14 10:27
 * @version 1.0
 */
@Configuration
public class Knife4jConfig {
    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI()
            .info(new Info()
                .title("candy-admin-api-doc")
                .version("1.0")
                .description("candy后台管理接口文档")
                .termsOfService("https://www.candy.com")
                .contact(new Contact().name("rong xi")
                .url("https://www.candy.com")
                .email("rongxineo@gmail.com"))
            );
    }
}