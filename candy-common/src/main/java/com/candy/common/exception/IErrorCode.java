package com.candy.common.exception;

/**
 * 错误编码接口
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
public interface IErrorCode {

    String getCode();

    String getDesc();

}
