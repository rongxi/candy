package com.candy.common.exception;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.hutool.core.util.StrUtil;
import com.candy.common.domain.Result;
import com.candy.common.enums.AdminErrorEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.Iterator;

/**
 * 通用返回结果
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 登录异常拦截
     *
     * @param e 异常
     * @return 返回对象
     */
    @ExceptionHandler(NotLoginException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public Result<String> handleNotLoginException(NotLoginException e) {
        log.error("登录异常-"+e.getMessage(), e);
        return Result.fail(HttpStatus.UNAUTHORIZED.value(),AdminErrorEnum.NOT_LOGIN,e.getMessage());
    }

    /**
     * 权限异常拦截
     *
     * @param e 异常
     * @return 返回对象
     */
    @ExceptionHandler(NotPermissionException.class)
    @ResponseStatus(value = HttpStatus.OK)
    public Result<String> handleNotPermissionException(NotPermissionException e) {
        log.error("权限异常-"+e.getMessage(), e);
        return Result.fail(AdminErrorEnum.NOT_PERMISSION);
    }

    /**
     * 业务异常拦截
     *
     * @param  e 业务异常
     * @return 返回对象
     */
    @ExceptionHandler(BizException.class)
    @ResponseStatus(value = HttpStatus.OK)
    public Result<?> handleServiceException(BizException e) {
        log.error("业务异常-"+e.getMessage(), e);
        return Result.fail(e.getMessage());
    }

    /**
     * 参数异常拦截
     *
     * @param e 参数异常
     * @return 返回对象
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.OK)
    public Result<String> handleBindException(BindException e) {
        String msg = null;

        Iterator<FieldError> iterator = e.getBindingResult().getFieldErrors().iterator();
        if (iterator.hasNext()) {
            FieldError fieldError = iterator.next();
            String paramName = fieldError.getField();
            if(StrUtil.isNotBlank(fieldError.getDefaultMessage())){
                msg = fieldError.getDefaultMessage();
            }else{
                msg = paramName + "参数异常!";
            }
        }
        log.error("参数异常-"+e.getMessage());
        return Result.fail(HttpStatus.BAD_REQUEST.value(),msg);
    }


    /**
     * 异常拦截
     *
     * @param e 异常
     * @return 返回对象
     */
    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.OK)
    public Result<?> handlerException(Exception e) {
        log.error("异常:"+e.getMessage(), e);
        return Result.fail(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage());
    }

}
