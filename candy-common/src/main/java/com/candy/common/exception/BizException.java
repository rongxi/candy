package com.candy.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * 业务异常
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public final class BizException extends RuntimeException {

    private String code;

    private String message;

    public static BizException create(String code, String msg) {
        return new BizException(code, msg);
    }


    public static BizException create(@NonNull IErrorCode errorEnum) {
        String msg = errorEnum.getDesc();
        return new BizException(errorEnum.getCode(), msg);
    }

    public static BizException create(@NonNull IErrorCode errorEnum, Object... params) {
        String msg = String.format(errorEnum.getDesc(), params);
        return new BizException(errorEnum.getCode(),msg);
    }

    public BizException(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

}
