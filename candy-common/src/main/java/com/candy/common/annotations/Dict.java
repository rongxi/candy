package com.candy.common.annotations;


import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Dict {

    /**
     * 枚举类
     */
    Class<?> dictEnum() default Object.class;

    /**
     * 枚举里面获取key的方法
     */
    String getValueMethod() default "getRemarkByValue";

    /**
     * 字典code
     */
    String value() default "";

}

