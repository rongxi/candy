package com.candy.common.annotations;

import java.lang.annotation.*;

/**
 * 重复提交校验
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/11/06 15:55
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Idempotent {

    /**
     * 间隔时间(ms)，小于此时间视为重复提交
     */
    int interval() default 2000;

    /**
     * 提示消息
     */
    String msg() default "操作过于频繁";
}
