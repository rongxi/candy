package com.candy.common.annotations;

import java.lang.annotation.*;

/**
 * 分布式锁
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lock {

    /**
     * 唯一标识
     */
    String value() default "";

    /**
     * key spel
     */
    String key() default "";

    /**
     * 等待锁超时时间(毫秒)
     */
    long waitTime() default 5*1000L;

    /**
     * 锁超时释放时间
     */
    long leaseTime() default 10*1000L;

}
