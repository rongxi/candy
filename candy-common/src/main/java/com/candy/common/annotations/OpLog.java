package com.candy.common.annotations;

import com.candy.common.enums.LogActionEnum;

import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpLog {

    /**
     * 操作类型
     */
    LogActionEnum action() default LogActionEnum.OTHER;

    /**
     * 模块
     */
    String module() default "";

    /**
     * 操作内容
     */
    String describe() default "";

    /**
     * 是否保存请求的参数
     */
    boolean saveRequest() default true;

    /**
     * 是否保存响应的参数
     */
    boolean saveResponse() default true;

}

