package com.candy.common.manager;


import cn.hutool.core.collection.CollectionUtil;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 缓存管理器
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@SuppressWarnings(value = {"unchecked", "rawtypes"})
@Component
@AllArgsConstructor
public class RedisManager {

    private static RedisManager manager;
    private final RedisTemplate redisTemplate;

    @PostConstruct
    private void init(){
        manager = this;
    }

    public static <T> void setValue(@NonNull String key, final T value) {
        manager.redisTemplate.opsForValue().set(key, value);
    }

    public static  <T> void setValue(@NonNull String key, final T value, final Long timeout, final TimeUnit timeUnit) {
        manager.redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    public static  <T> void setValue(@NonNull String key, final T value, final Long timeout) {
        manager.redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    public static boolean expire(@NonNull String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    public static boolean expire(@NonNull String key, final long timeout, final TimeUnit unit) {
        return Boolean.TRUE.equals(manager.redisTemplate.expire(key, timeout, unit));
    }

    public static long getExpire(@NonNull String key) {
        return Objects.requireNonNull(manager.redisTemplate.getExpire(key));
    }

    public static Boolean hasKey(@NonNull String key) {
        return manager.redisTemplate.hasKey(key);
    }

    public static <T> T getValue(@NonNull String key) {
        ValueOperations<String, T> operation = manager.redisTemplate.opsForValue();
        return operation.get(key);
    }

    public static <T> Optional<T> getValueOpt(@NonNull String key) {
        ValueOperations<String, T> operation = manager.redisTemplate.opsForValue();
        return Optional.ofNullable(operation.get(key));
    }

    public static boolean delete(String key) {
        return Boolean.TRUE.equals(manager.redisTemplate.delete(key));
    }

    public static  <T> long setList(@NonNull String key, final List<T> dataList) {
        Long count = manager.redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    public static  <T> List<T> getList(@NonNull String key) {
        return manager.redisTemplate.opsForList().range(key, 0, -1);
    }

    public static  <T> BoundSetOperations<String, T> setSet(@NonNull String key, final Set<T> dataSet) {
        BoundSetOperations<String, T> setOperation = manager.redisTemplate.boundSetOps(key);
        for (T t : dataSet) {
            setOperation.add(t);
        }
        return setOperation;
    }

    public static <T> Set<T> getSet(@NonNull String key) {
        return manager.redisTemplate.opsForSet().members(key);
    }

    public static <T> void setMap(@NonNull String key, final Map<String, T> dataMap) {
        if (CollectionUtil.isNotEmpty(dataMap)) {
            manager.redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }

    public static <T> Map<String, T> getMap(@NonNull String key) {
        return manager.redisTemplate.opsForHash().entries(key);
    }

    public static <T> Optional<Map<String, T>> getMapOpt(@NonNull String key) {
        return Optional.of(manager.redisTemplate.opsForHash().entries(key));
    }

    public static <T> void setMapValue(@NonNull String key, final String hKey, final T value) {
        manager.redisTemplate.opsForHash().put(key, hKey, value);
    }

    public static <T> T getMapValue(@NonNull String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = manager.redisTemplate.opsForHash();
        return opsForHash.get(key, hKey);
    }

    public static <T> Optional<T> getMapValueOpt(@NonNull String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = manager.redisTemplate.opsForHash();
        return Optional.ofNullable(opsForHash.get(key, hKey));
    }

    public static <T> List<T> getMultiMapValue(@NonNull String key, final Collection<Object> hKeys) {
        return manager.redisTemplate.opsForHash().multiGet(key, hKeys);
    }

    private static Collection<String> keys(@NonNull String key) {
        return manager.redisTemplate.keys(key);
    }

    public static long incr(@NonNull String key, long by) {
        return Objects.requireNonNull(manager.redisTemplate.opsForValue().increment(key, by));
    }
    public static double incr(@NonNull String key, double by) {
        return Objects.requireNonNull(manager.redisTemplate.opsForValue().increment(key, by));
    }

    public static long hincr(@NonNull String key,String item, long by) {
        return manager.redisTemplate.opsForHash().increment(key, item,by);
    }
    public static double hincr(@NonNull String key,String item, double by) {
        return manager.redisTemplate.opsForHash().increment(key,item, by);
    }

    private static long delete(@NonNull final Collection collection) {
        return Objects.requireNonNull(manager.redisTemplate.delete(collection));
    }

    public static void deleteByPrefix(@NonNull String prefix) {
        Collection<String> keys = keys(prefix + "*");
        delete(keys);
    }

    public  static  <T> Optional<T> getOpt(@NonNull Object key) {
        ValueOperations<String, T> operations = manager.redisTemplate.opsForValue();
        return Optional.ofNullable(operations.get(key.toString()));
    }

}
