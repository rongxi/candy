package com.candy.common.manager;

import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.ThreadFactory;

/**
 * 异步任务管理器
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@Slf4j
public class AsyncManager {
    private static final ThreadFactory VIRTUAL_THREAD_FACTORY = Thread.ofVirtual()
            .name("virtual-thread-", 0)
            .uncaughtExceptionHandler((thread,ex)-> log.error(thread.getName()+thread.threadId()+"执行异步任务出现异常:",ex))
            .factory();

    public static void runTask(Runnable runnable){
        VIRTUAL_THREAD_FACTORY.newThread(runnable).start();
    }

}
