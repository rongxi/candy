package com.candy.common.constant;

/**
 * 通用常量
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
public interface CommonConstant {

    Integer NO = 0;
    Integer YES = 1;

    Integer ZERO = 0;

    Integer ONE = 1;

    Integer TWO = 2;

    /**
     * 正序值
     */
    String ORDER_ASC = "ASC";

    /**
     * 倒叙值
     */
    String ORDER_DESC = "DESC";

    /**
     * 默认排序属性
     */
    String DEFAULT_ORDER_FIELD = "createTime";
    /**
     * 默认排序字段
     */
    String DEFAULT_ORDER_COLUMN = "create_time";

    /**
     * 默认创建人字段
     */
    String DEFAULT_CREATE_USER_ID = "createUser";

    /**
     * 默认修改人字段
     */
    String DEFAULT_UPDATE_USER_ID = "updateUser";

    /**
     * 默认每页条数
     */
    Integer DEFAULT_PAGE_SIZE = 10;

    /**
     * 最大分页页数
     */
    Integer MAX_PAGE_SIZE = 10000;

    /**
     * XSS排查路径参数
     */
    String XSS_EXCLUDES_PARAM = "xssExcludes";


}
