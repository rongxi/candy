package com.candy.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 请求操作类型
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/08 11:01
 */
@AllArgsConstructor
@Getter
public enum LogActionEnum {
    LOGIN("登录"),
    LOGOUT("退出"),
    OTHER("其他"),
    QUERY("查询"),
    INSERT("新增"),
    UPDATE("修改"),
    DELETE("删除"),
    SAVE("保存"),
    EXPORT("导出");

    private final String title;

}
