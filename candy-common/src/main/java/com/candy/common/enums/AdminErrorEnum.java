

package com.candy.common.enums;

import com.candy.common.exception.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 后台管理错误枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/09/14 18:28
 */
@AllArgsConstructor
@Getter
public enum AdminErrorEnum implements IErrorCode {

    /**
     * 00-系统错误
     */

    NOT_LOGIN("00-001","登录异常-%s"),
    NOT_PERMISSION("00-002","无此权限"),
    CAPTCHA_ERROR("00-003","验证码错误"),
    LOGIN_ERROR("00-004","用户名或密码错误"),
    CAPTCHA_REQUIRE_ERROR("00-005","请输入验证码"),
    CLIENT_ID_REQUIRE_ERROR("00-006","客户端id不能为空"),
    OPERATE_TOO_FREQUENTLY("00-007","操作频繁,请稍后再试"),
    /**
     * 01-业务错误
     */
    USER_DISABLED("01-001","该用户已被禁用"),
    USER_ALREADY_EXISTS("01-002", "用户已存在"),
    DICT_ALREADY_EXISTS("01-003", "字典已存在"),
    ROLE_ALREADY_EXISTS("01-004", "角色已存在"),
    USER_NOT_EXISTS("01-005", "用户不存在"),
    SYSTEM_USER_CANT_NOT_DELETE("01-006", "系统用户无法删除"),
    ROLE_NOT_EXISTS("01-007", "角色不存在"),
    SYSTEM_ROLE_CANT_NOT_DELETE("01-008", "系统角色无法删除"),
    CONFIG_NOT_EXISTS("01-008", "配置不存在"),
    SYSTEM_CONFIG_CANT_NOT_DELETE("01-009", "系统配置无法删除"),
    DICT_NOT_EXISTS("01-010", "字典不存在"),
    SYSTEM_DICT_CANT_NOT_DELETE("01-011", "系统字典无法删除"),

    ;

    private final String code;
    private final String desc;

}
