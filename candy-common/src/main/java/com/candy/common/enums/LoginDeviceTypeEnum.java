package com.candy.common.enums;

/**
 * 登录设备枚举
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/08 11:01
 */
public enum LoginDeviceTypeEnum {
    PC,H5,WECHAT
}
