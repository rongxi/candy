package com.candy.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

/**
 * redis key 枚举类
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/18 09:35
 */
@AllArgsConstructor
@Getter
public enum RedisKeyEnum {
    /**
     * 验证码
     */
    CAPTCHA("captcha",600L),
    /**
     * 用户信息
     */
    USER_INFO("user",86400L),
    /**
     * 字典
     */
    DICT_MAP("dict",86400L),
    /**
     * 系统配置
     */
    CONFIG("config",86400L),

    /**
     * 登录失败次数
     */
    LOGIN_FAIL_NUM("day-clean:login-fail-num",86400L),

    /**
     * 请求url 防重复提交使用
     */
    REQUEST_URL("request_url",1L),
    ;


    /**
     * redisHey
     */
    private final String key;

    /**
     * key失效秒数
     */
    private final Long timeout;

    public String getKey(Object param){
        return key +":"+Optional.ofNullable(param).map(Object::toString).orElse("");
    }

}
