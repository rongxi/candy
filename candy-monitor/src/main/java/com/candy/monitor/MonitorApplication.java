package com.candy.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * 监控启动类
 *
 * @author rong xi
 * @version 1.0
 * @date 2023/10/31 13:54
 */
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
@Slf4j
public class MonitorApplication {
    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(MonitorApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = Optional.ofNullable(env.getProperty("server.servlet.context-path")).orElse("");
        log.info("\n-----------------------------------------------------------------------\n\t" +
                "candy-monitor is running! access urls:\n\t" +
                "local: \t\thttp://localhost:" + port + path + "\n\t" +
                "external: \thttp://" + ip + ":" + port + path + "\n\t" +
                "-----------------------------------------------------------------------");
    }
}