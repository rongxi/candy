/*
 Navicat Premium Data Transfer

 Source Server         : candy
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : 60.204.159.136:3306
 Source Schema         : candy

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 14/11/2023 18:19:59
*/
DROP DATABASE IF EXISTS `candy`;

CREATE DATABASE  `candy` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

USE `candy`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
                              `id` bigint UNSIGNED NOT NULL COMMENT '编号',
                              `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '表名称',
                              `table_comment` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '表描述',
                              `table_schema` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库名称',
                              `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
                              `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
                              `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实体类名称',
                              `tpl_category` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
                              `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成包路径',
                              `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成模块名',
                              `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成业务名',
                              `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成功能名',
                              `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成功能作者',
                              `gen_type` tinyint NULL DEFAULT 0 COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
                              `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
                              `options` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
                              `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                              `create_user` bigint NOT NULL COMMENT '创建者',
                              `create_time` datetime NOT NULL COMMENT '创建时间',
                              `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                              `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                              `parent_menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上级菜单名称',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
                                     `id` bigint UNSIGNED NOT NULL COMMENT '编号',
                                     `table_id` bigint NOT NULL COMMENT '归属表编号',
                                     `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表名称',
                                     `column_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列名称',
                                     `column_name_upper` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称大写',
                                     `column_comment` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列描述',
                                     `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '列类型',
                                     `java_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'JAVA类型',
                                     `java_field` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'JAVA字段名',
                                     `is_pk` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否主键（1是）',
                                     `is_increment` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否自增（1是）',
                                     `is_required` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否必填（1是）',
                                     `is_insert` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为插入字段（1是）',
                                     `is_edit` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否编辑字段（1是）',
                                     `is_list` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否列表字段（1是）',
                                     `is_query` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否查询字段（1是）',
                                     `is_verify` tinyint NULL DEFAULT 0 COMMENT '是否需要验证',
                                     `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
                                     `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
                                     `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `order_num` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '排序',
                                     `create_user` bigint NOT NULL COMMENT '创建者',
                                     `create_time` datetime NOT NULL COMMENT '创建时间',
                                     `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                                     `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                     `get_method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'get方法',
                                     `set_method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'set方法',
                                     `content_length` int UNSIGNED NULL DEFAULT NULL COMMENT '内容长度',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `id` bigint UNSIGNED NOT NULL COMMENT '参数主键',
                               `config_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数名称',
                               `config_key` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数键名',
                               `config_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数键值',
                               `type` tinyint NOT NULL DEFAULT 1 COMMENT '类型(0系统参数 1普通参数)',
                               `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                               `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志（0代表存在 1代表删除）',
                               `create_user` bigint NOT NULL COMMENT '创建人',
                               `create_time` datetime NOT NULL COMMENT '创建时间',
                               `update_user` bigint NULL DEFAULT NULL COMMENT '更新人',
                               `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1706610992399376384, 'token失效小时数', 'token-timeout-hour', '12', 1, '222', 0, 1704406404460019712, '2023-09-26 18:05:43', 1704406404460019712, '2023-09-26 18:06:19');
INSERT INTO `sys_config` VALUES (1711001186397429760, '登录失败禁用次数', 'loginFailDisableNum', '5', 0, '登录失败禁用次数', 0, 1704406404460019712, '2023-10-08 20:50:46', 1704406404460019712, '2023-10-27 11:42:18');
INSERT INTO `sys_config` VALUES (1716693662797996032, '是否开启验证码', 'enableCaptcha', '0', 0, NULL, 0, 1704406404460019712, '2023-10-24 13:50:39', 1704406404460019712, '2023-11-14 18:07:53');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `id` bigint NOT NULL COMMENT 'id',
                             `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
                             `parent_id` bigint NOT NULL COMMENT '上级部门',
                             `order_num` tinyint NULL DEFAULT 0 COMMENT '显示顺序',
                             `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志（0正常 1已删除）',
                             `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `create_user` bigint NOT NULL COMMENT '创建者',
                             `create_time` datetime NOT NULL COMMENT '创建时间',
                             `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1717434421872467968, '人力资源部', 1717443373129658368, 0, 0, NULL, 1704406404460019712, '2023-10-26 14:54:09', 1704406404460019712, '2023-10-26 15:29:53');
INSERT INTO `sys_dept` VALUES (1717437708759855104, '地球事务管理委员会', 0, 0, 0, '管理地球事务', 1704406404460019712, '2023-10-26 15:07:13', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717438827636584448, '亚洲事务管理委员会', 1717437708759855104, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:11:40', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717442314277613568, '中国事务管理委员会', 1717438827636584448, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:25:31', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717443136512192512, '山东事务管理委员会', 1717442314277613568, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:28:47', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717443373129658368, '阳光集团', 0, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:29:43', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717443568974295040, '综合部', 1717443373129658368, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:30:30', NULL, NULL);
INSERT INTO `sys_dept` VALUES (1717443788470611968, '财务部', 1717443373129658368, 0, 0, NULL, 1704406404460019712, '2023-10-26 15:31:22', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
                             `id` bigint NOT NULL COMMENT 'id',
                             `dict_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编号',
                             `dict_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
                             `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志（0代表存在 1代表删除）',
                             `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `type` tinyint NULL DEFAULT 1 COMMENT '类型(0系统,1普通)',
                             `create_user` bigint NOT NULL COMMENT '创建人',
                             `create_time` datetime NOT NULL COMMENT '创建时间',
                             `update_user` bigint NULL DEFAULT NULL COMMENT '更新人',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `dict_code`(`dict_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1714575555493122048, 'log_type', '操作日志类型', 0, NULL, 0, 1704406404460019712, '2023-10-18 17:34:02', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
                                  `id` bigint NOT NULL COMMENT 'id',
                                  `dict_id` bigint NOT NULL COMMENT '字典id',
                                  `dict_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典编号',
                                  `item_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典项名',
                                  `item_value` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典项值',
                                  `order_num` tinyint NOT NULL DEFAULT 0 COMMENT '显示顺序',
                                  `state` tinyint NOT NULL DEFAULT 0 COMMENT '状态（0正常 1停用）',
                                  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  `create_user` bigint NOT NULL COMMENT '创建人',
                                  `create_time` datetime NOT NULL COMMENT '创建时间',
                                  `update_user` bigint NULL DEFAULT NULL COMMENT '更新人',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典项' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1718243559724666880, 1714575555493122048, 'log_type', '登录', 'LOGIN', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243559808552960, 1714575555493122048, 'log_type', '退出', 'LOGIN_OUT', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243559884050432, 1714575555493122048, 'log_type', '新增', 'ADD', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243559967936512, 1714575555493122048, 'log_type', '删除', 'REMOVE', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243560043433984, 1714575555493122048, 'log_type', '修改', 'EDIT', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243560127320064, 1714575555493122048, 'log_type', '查询', 'QUERY', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243560202817536, 1714575555493122048, 'log_type', '其他', 'OTHER', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (1718243560286703616, 1714575555493122048, 'log_type', '保存', 'SAVE', 0, 0, NULL, 1704406404460019712, '2023-10-28 20:29:23', NULL, NULL);

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
                            `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
                            `module_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '模块',
                            `type` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '操作类型(登录,退出,新增,修改,保存,删除,其他)',
                            `describe` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '描述',
                            `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
                            `user_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '用户名称',
                            `request_ip` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '请求ip',
                            `request_region` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'ip归属地',
                            `request_method` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '请求方式',
                            `request_param` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '请求参数',
                            `request_url` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '请求url',
                            `request_time` datetime NULL DEFAULT NULL COMMENT '请求时间',
                            `request_token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '请求token',
                            `client_browser` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '浏览器',
                            `client_os` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '操作系统',
                            `cost_time` bigint NULL DEFAULT NULL COMMENT '耗时(毫秒)',
                            `error_msg` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '错误信息',
                            `response_content` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '返回内容',
                            `create_user` bigint NOT NULL COMMENT '创建者',
                            `create_time` datetime NOT NULL COMMENT '创建时间',
                            `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                            `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                            `user_account` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '用户账号',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1799 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = '操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `id` bigint UNSIGNED NOT NULL COMMENT 'id',
                             `menu_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                             `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
                             `parent_id` bigint NULL DEFAULT 0 COMMENT '父级id',
                             `type` tinyint NOT NULL COMMENT '类型(1菜单2权限)',
                             `route_path` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址',
                             `component_path` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
                             `permission_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                             `order_num` tinyint NULL DEFAULT 0 COMMENT '显示顺序',
                             `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志（0正常 1已删除）',
                             `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `create_user` bigint NOT NULL COMMENT '创建者',
                             `create_time` datetime NOT NULL COMMENT '创建时间',
                             `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `route-idx`(`route_path`) USING BTREE,
                             UNIQUE INDEX `per-idx`(`permission_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1710578984220983211, '查询权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:list', 1, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983212, '新增权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:add', 2, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983213, '修改权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:edit', 3, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983214, '删除权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:remove', 4, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983215, '导出权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:export', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983216, '重置密码权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:reset-pwd', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:08:09', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983217, '分配角色权限', NULL, 1710578984220983292, 2, NULL, NULL, 'sys:user:roles', 6, 0, NULL, 1704406404460019712, '2023-10-10 22:11:21', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983218, '详情权限', '', 1710578984220983292, 2, NULL, NULL, 'sys:user:info', 7, 0, NULL, 1704406404460019712, '2023-09-27 10:38:27', 1704406404460019712, '2023-10-07 17:17:14');
INSERT INTO `sys_menu` VALUES (1710578984220983221, '查询权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:list', 1, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983222, '新增权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:add', 2, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983223, '修改权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:edit', 3, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983224, '删除权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:remove', 4, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983225, '导出权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:export', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983226, '分配菜单权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:menu', 6, 0, NULL, 1704406404460019712, '2023-10-10 22:11:21', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983227, '详情权限', NULL, 1710578984220983295, 2, NULL, NULL, 'sys:role:info', 6, 0, NULL, 1704406404460019712, '2023-10-10 22:11:21', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983231, '查询权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:list', 1, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983232, '新增权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:add', 2, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983233, '修改权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:edit', 3, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983234, '删除权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:remove', 4, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983235, '导出权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:export', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983236, '详情权限', NULL, 1710578984220983293, 2, NULL, NULL, 'sys:menu:info', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983241, '查询权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:list', 1, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983242, '新增权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:add', 2, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983243, '修改权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:edit', 3, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983244, '删除权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:remove', 4, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983245, '导出权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:export', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983246, '详情权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:info', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983247, '字典项权限', NULL, 1710578984220983296, 2, NULL, NULL, 'sys:dict:item', 6, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983251, '查询权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:list', 1, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983252, '新增权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:add', 2, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983253, '修改权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:edit', 3, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983254, '删除权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:remove', 4, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983255, '导出权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:export', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983256, '详情权限', NULL, 1710578984220983297, 2, NULL, NULL, 'sys:config:info', 5, 0, NULL, 1704406404460019712, '2023-10-10 22:06:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1710578984220983291, '系统管理', '', 0, 1, '/system', NULL, NULL, 1, 0, NULL, 1704406404460019712, '2023-09-27 10:36:23', 1704406404460019712, '2023-10-11 11:40:19');
INSERT INTO `sys_menu` VALUES (1710578984220983292, '用户管理', 'layui-icon-username', 1710578984220983291, 1, '/system/user', NULL, NULL, 0, 0, NULL, 1704406404460019712, '2023-09-27 10:36:54', 1704406404460019712, '2023-10-11 11:34:06');
INSERT INTO `sys_menu` VALUES (1710578984220983293, '菜单管理', 'layui-icon-spread-left', 1710578984220983291, 1, '/system/menu', NULL, NULL, 2, 0, NULL, 1704406404460019712, '2023-09-27 10:37:46', 1704406404460019712, '2023-10-11 11:49:40');
INSERT INTO `sys_menu` VALUES (1710578984220983295, '角色管理', 'layui-icon-user', 1710578984220983291, 1, '/system/role', NULL, NULL, 1, 0, NULL, 1704406404460019712, '2023-09-27 10:39:09', 1704406404460019712, '2023-10-11 11:49:23');
INSERT INTO `sys_menu` VALUES (1710578984220983296, '字典管理', 'layui-icon-read', 1710578984220983291, 1, '/system/dictionary', NULL, NULL, 3, 0, NULL, 1704406404460019712, '2023-10-07 16:53:06', 1704406404460019712, '2023-10-11 11:49:50');
INSERT INTO `sys_menu` VALUES (1710578984220983297, '配置管理', 'layui-icon-set-sm', 1710578984220983291, 1, '/system/config', NULL, NULL, 4, 0, NULL, 1704406404460019712, '2023-10-07 16:53:06', 1704406404460019712, '2023-10-11 11:49:59');
INSERT INTO `sys_menu` VALUES (1712026224126951424, '操作日志', 'layui-icon-form', 1710578984220983291, 1, '/system/log', NULL, NULL, 5, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', 1704406404460019712, '2023-10-11 17:26:15');
INSERT INTO `sys_menu` VALUES (1712026224126951425, '系统操作日志查询', NULL, 1712026224126951424, 2, NULL, NULL, 'sys:log:list', 1, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1712026224126951426, '系统操作日志详情', NULL, 1712026224126951424, 2, NULL, NULL, 'sys:log:info', 2, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1712026224126951427, '在线用户查询', NULL, 1712026224126951429, 2, NULL, NULL, 'sys:online:list', 1, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1712026224126951428, '在线用户强退', NULL, 1712026224126951429, 2, NULL, NULL, 'sys:online:tickout', 2, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1712026224126951429, '在线用户', 'layui-icon-group', 1710578984220983291, 1, '/system/online', NULL, NULL, 5, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', 1704406404460019712, '2023-11-14 18:14:13');
INSERT INTO `sys_menu` VALUES (1712026224126951430, '系统操作日志导出', NULL, 1712026224126951424, 2, NULL, NULL, 'sys:log:export', 6, 0, NULL, 1704406404460019712, '2023-10-11 16:45:33', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254976, '部门管理', 'layui-icon-tree', 1710578984220983291, 1, '/system/dept', NULL, NULL, 1, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', 1704406404460019712, '2023-10-26 10:58:49');
INSERT INTO `sys_menu` VALUES (1717372154032254977, '部门查询', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:list', 1, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254978, '部门详情', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:info', 2, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254979, '部门新增', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:add', 3, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254980, '部门修改', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:edit', 4, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254981, '部门删除', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:remove', 5, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES (1717372154032254982, '部门导出', NULL, 1717372154032254976, 2, NULL, NULL, 'biz:dept:export', 6, 0, NULL, 1704406404460019712, '2023-10-26 10:48:18', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` bigint NOT NULL COMMENT '角色ID',
                             `role_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色标识',
                             `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
                             `order_num` int NOT NULL DEFAULT 0 COMMENT '显示顺序',
                             `type` tinyint NULL DEFAULT 1 COMMENT '角色类型(0系统角色,1普通角色)',
                             `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志（0代表存在 1代表删除）',
                             `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `create_user` bigint NOT NULL COMMENT '创建者',
                             `create_time` datetime NOT NULL COMMENT '创建时间',
                             `update_user` bigint NULL DEFAULT NULL COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1706504001018511360, 'admin', '管理员', 0, 1, 0, '不要删除啊', 1704406404460019712, '2023-09-26 11:00:34', NULL, NULL);
INSERT INTO `sys_role` VALUES (1706504118610018304, 'superAdmin', '超级管理员', 0, 0, 0, '不要删除啊', 1704406404460019712, '2023-09-26 11:01:02', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722147598665326592, '22', '22', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:02:38', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722150642534662144, '33', '33', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:14:44', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722152059869655040, '222', '222', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:20:22', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722154578461126656, '2222', '2222', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:30:22', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722154702629302272, '111', '111', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:30:52', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722154755771133952, '555555', '444', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:31:05', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722156264600727552, '1212', '1212', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 15:37:04', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722170071804854272, '3123213', '12313', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 16:31:56', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722173015702982656, '123112', '123123', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 16:43:38', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722177058030567424, '11', '11', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 16:59:42', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722177418111561728, '123132', '2313', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:01:08', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722177418111561729, '123132', '2313', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:01:08', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722177418111561730, '123132', '2313', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:01:08', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722178015955070976, '3333', '34444', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:03:30', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722178081117777920, '1111', '2121', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:03:46', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722178126638559232, '232131', '2222', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:03:57', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722180057238941696, '66666', '66666', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 17:11:37', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722180342040571904, '66666', '66666', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 17:12:45', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722180342044766208, '66666', '66666', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 17:12:45', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722180342057349120, '66666', '66666', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 17:12:45', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722188774793646080, '66666', '66666', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 17:46:15', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722189002171060224, '777', '777', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:47:10', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722189035528359936, '888', '88', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 17:47:17', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722192872561532928, '77777', '77777', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 18:02:32', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722193011619487744, '999', '999', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 18:03:05', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722193085711867904, '77777', '77777', 0, 1, 1, NULL, 1704406404460019712, '2023-11-08 18:03:23', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722193690199154688, '66666', '66666', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 18:05:47', NULL, NULL);
INSERT INTO `sys_role` VALUES (1722193692497633280, '77777', '77777', 0, 1, 0, NULL, 1704406404460019712, '2023-11-08 18:05:48', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                  `role_id` bigint NOT NULL COMMENT '角色ID',
                                  `menu_id` bigint NOT NULL COMMENT '菜单ID',
                                  `create_user` bigint NOT NULL COMMENT '创建人',
                                  `create_time` datetime NOT NULL COMMENT '创建时间',
                                  `update_user` bigint NULL DEFAULT NULL COMMENT '更新人',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `uni-idex`(`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711929767692673036 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1711929767692673024, 1706504001018511360, 0, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673025, 1706504001018511360, 1710578984220983291, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673026, 1706504001018511360, 1710578984220983296, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673027, 1706504001018511360, 1710578984220983241, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673028, 1706504001018511360, 1710578984220983297, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673029, 1706504001018511360, 1710578984220983251, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673030, 1706504001018511360, 1710578984220983295, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673031, 1706504001018511360, 1710578984220983221, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673032, 1706504001018511360, 1710578984220983293, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673033, 1706504001018511360, 1710578984220983231, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673034, 1706504001018511360, 1710578984220983292, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);
INSERT INTO `sys_role_menu` VALUES (1711929767692673035, 1706504001018511360, 1710578984220983211, 1704406404460019712, '2023-10-11 10:20:37', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` bigint NOT NULL COMMENT '用户ID',
                             `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
                             `login_account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
                             `login_password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                             `dept_id` bigint NULL DEFAULT NULL COMMENT '所属部门',
                             `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '盐加密',
                             `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
                             `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
                             `sex` tinyint NULL DEFAULT 0 COMMENT '用户性别（0男 1女 2未知）',
                             `type` tinyint NULL DEFAULT 1 COMMENT '用户类型（0超管用户 1普通用户）',
                             `state` tinyint NOT NULL DEFAULT 0 COMMENT '帐号状态（0正常 1停用）',
                             `del_flag` tinyint NOT NULL DEFAULT 0 COMMENT '删除标志（0代表存在 1代表删除）',
                             `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `create_user` bigint NOT NULL COMMENT '创建人',
                             `create_time` datetime NOT NULL COMMENT '创建时间',
                             `update_user` bigint NULL DEFAULT NULL COMMENT '修改人',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `uni_idx`(`del_flag`, `login_account`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1704406404460019712, '超级管理员', 'admin', '349f7456f6b0c3a6fc7500b587baa945', 0, '9un9ge6p', '18888888888', 'rongxineo@gmail.com', 0, 0, 0, 0, '超级管理员，都不要动他', 1704406404460019712, '2023-09-20 16:05:28', NULL, NULL);
INSERT INTO `sys_user` VALUES (1705981223118938112, '融溪', 'rongxi', '5eb3cb95721bcee65045fdfefa638d02', 1717443373129658368, 'm6cgh4ca', '13366666666', 'rongxi@qq.com', 0, 1, 1, 0, 'rongxi2', 1704406404460019712, '2023-09-25 00:23:14', 1704406404460019712, '2023-10-27 09:58:04');
INSERT INTO `sys_user` VALUES (1717722572683788288, '张三', 'zhangsan', 'a6bbb28a8014c614050c9937a02dec70', 1717443788470611968, '1ysnfkda', '13300000000', '420154195@qq.com', 0, 1, 0, 0, NULL, 1704406404460019712, '2023-10-27 09:59:10', NULL, NULL);
INSERT INTO `sys_user` VALUES (1717723353768050688, '李四', 'lisi', '4fe11655e62f8d0304fc3e416ced5f41', 1717443568974295040, 'm7lpiqaa', NULL, NULL, 0, 1, 0, 0, NULL, 1704406404460019712, '2023-10-27 10:02:16', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                  `user_id` bigint NOT NULL COMMENT '用户ID',
                                  `role_id` bigint NOT NULL COMMENT '角色ID',
                                  `create_user` bigint NOT NULL COMMENT '创建人',
                                  `create_time` datetime NOT NULL COMMENT '创建时间',
                                  `update_user` bigint NULL DEFAULT NULL COMMENT '更新人',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `uni-idx`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1711929038680694785 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1710930891695308800, 1704406404460019712, 1706504118610018304, 1704406404460019712, '2023-10-08 16:11:27', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (1710954574719594496, 1706151730200006656, 1706504001018511360, 1704406404460019712, '2023-10-08 17:45:33', NULL, NULL);
INSERT INTO `sys_user_role` VALUES (1711929038680694784, 1705981223118938112, 1706504001018511360, 1704406404460019712, '2023-10-11 10:17:43', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
